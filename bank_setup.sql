-- Adds columns to the database.
ALTER TABLE bank_users 
ADD COLUMN savings_balance NUMERIC(15, 2) NOT NULL DEFAULT 0,
ADD COLUMN checking_balance NUMERIC(15, 2) NOT NULL DEFAULT 0;

-- Update existing users. Set balance to 5000 for both
-- accounts by default.
UPDATE bank_users
SET savings_balance = 5000,
    checking_balance = 5000;


-- Add a trigger that will create account balances
-- and default amounts into new users.
DELIMITER $$

CREATE TRIGGER new_account 
BEFORE INSERT ON bank_users 
FOR EACH ROW
BEGIN
  SET NEW.savings_balance = 5000;
  SET NEW.checking_balance = 5000;
END

$$

DELIMITER ;


-- Create a table to hold banking transactions.
CREATE TABLE bank_transactions (
  ID BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  sender_id BIGINT(20) UNSIGNED NOT NULL,
  sender_type ENUM('Savings', 'Checking') NOT NULL,
  recipient_id BIGINT(20) UNSIGNED NOT NULL, 
  recipient_type ENUM('Savings', 'Checking') NOT NULL,
  transaction_timestamp TIMESTAMP NOT NULL,
  description VARCHAR(255) NOT NULL DEFAULT '',
  amount NUMERIC(15, 2) NOT NULL,
  sender_balance NUMERIC(15, 2) NOT NULL DEFAULT 0,
  recipient_balance NUMERIC(15,2) NOT NULL DEFAULT 0,
  PRIMARY KEY(ID),
  FOREIGN KEY (sender_id) REFERENCES bank_users(ID),
  FOREIGN KEY (recipient_id) REFERENCES bank_users(ID));


-- Create a trigger to validate transactions and update balances.
DELIMITER $$

CREATE TRIGGER new_transaction
BEFORE INSERT ON bank_transactions
FOR EACH ROW
BEGIN
  DECLARE dummy INT;

  IF (NEW.sender_id = NEW.recipient_id AND NEW.sender_type = NEW.recipient_type) THEN
    SELECT `Cannot transfer money to the same account.`
    INTO dummy
    FROM bank_transactions
    WHERE bank_transactions = NEW.ID;
  END IF;

  IF (NEW.amount <= 0) THEN
    SELECT `Cannot insert transaction with an amount less than or equal to zero.`
    INTO dummy
    FROM bank_transactions
    WHERE bank_transactions = NEW.ID;
  END IF;

  SET NEW.sender_balance = (SELECT IF(NEW.sender_type = 'Savings', savings_balance, checking_balance)
                         FROM bank_users
                         WHERE ID = NEW.sender_id) - NEW.amount;
                                       
  SET NEW.recipient_balance = (SELECT IF(NEW.recipient_type = 'Savings', savings_balance, checking_balance)
                            FROM bank_users
                            WHERE ID = NEW.recipient_id) + NEW.amount;

  IF (NEW.sender_balance < 0) THEN
    SELECT `Cannot insert transaction that will put a balance below 0.`
    INTO dummy
    FROM bank_transactions
    WHERE bank_transactions = NEW.ID;
  END IF;

  UPDATE bank_users
  SET savings_balance = IF(NEW.sender_type = 'Savings', NEW.sender_balance, savings_balance),
      checking_balance = IF(NEW.sender_type = 'Checking', NEW.sender_balance, checking_balance)
  WHERE ID = NEW.sender_id;
  
  UPDATE bank_users
  SET savings_balance = IF(NEW.recipient_type = 'Savings', NEW.recipient_balance, savings_balance),
      checking_balance = IF(NEW.recipient_type = 'Checking', NEW.recipient_balance, checking_balance)
  WHERE ID = NEW.recipient_id;
END

$$

DELIMITER ;



