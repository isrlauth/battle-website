<?php

/**
 * Plugin Name: Forums Tester
 * Plugin URI: http://isrlresearch.byu.edu/
 * Description: A plugin that helps us test authentication on our forums. This plugin requires
 * that there is an administrative user named admin.
 * Author: Scott Ruoti
 * Author URI: http://isrl.byu.edu.com/
 * License: None
 * Network: true
 * Text Domain: formus-tester
 * Domain Path: /lang
 */

if (!class_exists('ForumsTester')) {
  class ForumsTester {

    /**
     * Singleton code.
     */
    private static $instance = null;

    public static function get_instance() {
      if (null == self::$instance) {
        self::$instance = new self;
      }
      return self::$instance;
    }

    /**
     * Constructer.
     */
    function __construct() {
      add_action('parse_request', array($this, 'logoutUser'));
      add_action('parse_request', array($this, 'deleteUser'));

      add_action('wp_login', array($this, 'userLoggedIn'), 10, 2);
      add_action('user_register', array($this, 'userRegistered'), 1);

      add_filter('login_redirect', array($this, 'loginRedirect'), 10, 3);

      add_action('the_title', array($this, 'removePrivateFromTitle'));

      add_action('publish_topic', array($this, 'respondToNewUser'));
    }

    /**
     * Sends a response that can be done cross domain.
     */
    private function sendCorsResponse($content = null) {
      if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header("Access-Control-Expose-Headers: x-json");
      } else {
        header('Access-Control-Allow-Origin: *');
      }

      if ($content) {
        header('Content-Type: application/json');
        echo $content;
      }

      exit;
    }

    /**
     * Log the user out.
     */
    public function logoutUser() {
      if (isset($_GET['logoutUser'])) {
        wp_logout();
        $this->sendCorsResponse();
      }
    }

    /**
     * Deletes the user.
     */
    public function deleteUser() {
      if (isset($_GET['deleteUser'])) {
        $wpSession = WP_Session::get_instance();
        if (isset($wpSession['ForumsTester_userId'])) {
          $user = get_userdata($wpSession['ForumsTester_userId']);
        }
        if (!($user && !is_wp_error($user))) {
          $user = wp_get_current_user();
        }

        // If we have a valid user back them up.
        if ($user && !is_wp_error($user) && $user->user_login != 'admin') {
          require_once(ABSPATH . 'wp-admin/includes/user.php');
          wp_delete_user($user->id);
          $this->sendCorsResponse();
        }
      }
    }

    /**
     * Automatically respond to new topics in the new user forum.
     */
    public function respondToNewUser($topicId) {
      $topicPost = get_post($topicId);
      $forum = get_post($topicPost->post_parent);

      if ($forum->post_type === 'forum' && $forum->post_title === 'New Users') {
        wp_insert_post(array(
            'post_content' => __('Welcome to Smartphone support! The code to finish this task is ') .
                rand(),
            'post_status'  => 'publish',
            'post_type'    => 'reply',
            'post_author'  => get_user_by('login', 'admin')->id, // Reply using the administrator.
            'post_parent'  => $topicId,
        ));
      }
    }

    /**
     * Send users an email with the code they need to complete the first forum task.
     */
    public function userRegistered($userId) {
      $wpSession = WP_Session::get_instance();
      $wpSession['ForumsTester_userId'] = $userId;

      // We do not send the e-mail for either Snap2PassAuth or WebTicketAuth. Both have another mechanism to
      // get this code.
      if(isset($wpSession["Snap2PassAuth_enabled"]) || isset($wpSession["WebTicketAuth_enabled"]))
        return;

      $code = rand() % 1000000000;
      while ($code <= 99999999)
        $code = rand() % 1000000000;

      // Send an email simmilar to wp_new_user_notification
      $user = get_userdata($userId);
      $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
      $message = __("You have successfully registered for the site with the username '{$user->user_login}'.\r\n");
      $message .= __("The code to finish this task is: ") . $code . "\r\n";
      $headers = "From: admin@forums.isrl.byu.edu.com" . "\r\n";

      wp_mail($user->user_email, sprintf(__('[%s] Registration successfully completed.'), $blogname), $message);
    }

    /**
     * Removes the word private from the title.
     */
    public function removePrivateFromTitle($title) {
      return str_replace('Private: ', '', $title);
    }

    /**
     * Send users an email with the code they need to complete the first forum task.
     */
    public function userLoggedIn($userLogin, $user) {
      $wpSession = WP_Session::get_instance();
      $wpSession['ForumsTester_userId'] = $user->id;
    }

    /**
     * Redirect the user to the main page.
     */
    public function loginRedirect($redirectTo, $request, $user) {
      return home_url();
    }

  }
}

/**
 * Starts ForumsTester.
 */
function ForumsTester() {
  return ForumsTester::get_instance();
}

ForumsTester();

/*
Plugin Name: Disable Lost Password Feature
*/
function disable_password_reset() { return false; }
add_filter ( 'allow_password_reset', 'disable_password_reset' );
add_filter ( 'show_password_fields', 'disable_password_reset' );

?>
