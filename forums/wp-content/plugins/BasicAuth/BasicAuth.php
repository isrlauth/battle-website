<?php

/**
 * Plugin Name: Basic Auth
 * Plugin URI: http://isrlresearch.byu.edu/
 * Description: A plugin that removes the need for passwords. Should never be used in production.
 * Version: 1.0
 * Author: Scott Ruoti
 * Author URI: http://isrl.byu.edu.com/
 * License: None
 * Network: true
 * Text Domain: basic-auth
 * Domain Path: /lang
 */

// Options page.
require_once 'BasicAuthSettings.php';

if (!class_exists('BasicAuth')) {
  require_once 'core/AuthCore.php';

  class BasicAuth extends AuthCore {

    /**
     * Singleton code.
     */
    private static $instance = null;

    public static function get_instance() {
      if (null == self::$instance) {
        self::$instance = new self;
      }
      return self::$instance;
    }

    /**
     * Constructer.
     */
    function __construct() {
      parent::__construct();
    }

    /**
     * Gets a name that identifies this plugin. Should be unique from all other
     * authentication plugins. Used in various functions to identify this plugin.
     * It is also used as a query variable and should be so compatible.
     */
    protected function getName() {
      return "BasicAuth";
    }

    /**
     * Gets the content to be put on the login page.
     */
    protected function getLoginFormContent() {
      if (isset($_POST['user_login'])) {
        $user_login = $_POST['user_login'];
      }
      $rememberme = !empty($_POST['rememberme']);
      ob_start();
      ?>

      <form name="BasicAuthForm" id="BasicAuthForm" action="<?php echo esc_url($this->getAuthenticateUrl()); ?>"
            method="post">
        <p>

        <h3>Log in with Basic Auth</h3></p><br/>

        <p>
          <label for="user_login"><?php _e('Username'); ?><br>
            <input type="text" name="user_login" id="basic_auth_user_login" class="input"
                   value="<?php echo esc_attr($user_login); ?>" size="20"></label>
        </p>

        <p class="forgetmenot">
          <input name="rememberme" type="checkbox" id="rememberme" value="forever" <?php checked($rememberme); ?> />
          <label for="rememberme"><?php esc_attr_e('Remember Me'); ?></label>
        </p>

        <p class="submit">
          <input type="submit" name="wp-submit" id="basic_auth_wp-submit" class="button button-primary button-large"
                 value="<?php esc_attr_e('Log In'); ?>">
        </p>
      </form>

      <script>
        // Grab the redirect_to field.
        jQuery(document).ready(function () {
          jQuery('form#loginform input[name="redirect_to"]').clone().insertAfter(jQuery('#basic_auth_wp-submit'));
        });
      </script>

      <?php
      return ob_get_clean();
    }

    /**
     * Authenticates the user.
     */
    protected function authenticateUser($user, $username = null, $password = null) {
      if (!empty($username)) {
        $user = get_user_by('login', $username);
        if (!$user) {
          return new WP_Error('invalid_username', __('<strong>ERROR</strong>: Invalid username.'));
        }
        if (!$this->getBasicAuthEnabled($user->ID)) {
          return new WP_Error('new_error',
              __('<strong>ERROR</strong>: BasicAuth not enabled for this account.'));
        }
        return $user;
      }
    }

    /**
     * Gets the content to be put on the registration page.
     */
    protected function getRegisterFormContent() {
      $user_login = $_POST['user_login'];

      ob_start();
      ?>

      <form name="BasicAuthForm" id="BasicAuthForm" action="<?php echo esc_url($this->getRegisterUrl()); ?>"
            method="post">
        <p>

        <h3>Register with Basic Auth</h3></p><br/>

        <p>
          <label for="user_login"><?php _e('Username') ?><br/></label>
          <input type="text" name="user_login" id="basic_auth_user_login" class="input"
                 value="<?php echo esc_attr(wp_unslash($user_login)); ?>" size="20"/>
        </p>

        <p class="submit">
          <input type="submit" name="wp-submit" id="basic_auth_wp-submit" class="button button-primary button-large"
                 value="<?php esc_attr_e('Register'); ?>">
        </p>
      </form>

      <?php
      return ob_get_clean();
    }

    /**
     * Authenticates the user with this authentication method.
     */
    protected function registerUser($username, $usermail, $errors) {
      // If there is a problem we cannot register it.
      $newErrors = new WP_Error();
      foreach (array('empty_username', 'invalid_username', 'username_exists') as $code) {
        $this->copyErrors($errors, $newErrors, $code);
      }
      if ($newErrors->get_error_code() != '') {
        return $newErrors;
      }

      $userpass = 'password';//wp_generate_password(12, false);
      $userId = wp_create_user($username, $userpass, 'blank');
      if (!$userId || is_wp_error($userId)) {
        return $userId;
        $newErrors->add('registerfail',
            sprintf(__('<strong>ERROR</strong>: Couldn\'t register you&hellip; please contact the <a href="mailto:%s">webmaster</a> !'),
                get_option('admin_email')));
        return $newErrors;
      }

      $this->setBasicAuthEnabled($userId, true);
      wp_new_user_notification($userId, $userpass);
      return $userId;
    }

    /**
     * Gets the content to be put on the user's profile page.
     */
    protected function getProfileContent($user) {
      ob_start();
      ?>

      <table class="form-table">
        <tbody>
        <tr>
          <th scope="row"><?php _e("Basic Auth") ?></th>
          <td>
            <input name="enableBasicAuth" id="enableBasicAuth" type="checkbox"
                <?php checked($this->getBasicAuthEnabled($user->ID)); ?> />
            <label for="enableBasicAuth"><?php _e("Enable Basic Auth for this account."); ?></label>
            <br/>
          </td>
        </tr>
        </tbody>
      </table>

      <?php
      return ob_get_clean();
    }

    /**
     * Updates the user's personal options.
     */
    protected function updatePersonalOptions($userId, $errors) {
      if (current_user_can('edit_user', $userId)) {
        $this->setBasicAuthEnabled($userId, isset($_POST['enabledBasicAuth']));
      }
    }

    /**
     * Sets an option indicating the giver user can use Basic Auth.
     */
    private function setBasicAuthEnabled($userId, $enabled) {
      update_user_meta($userId, "{$this->getName()}_enabled", $enabled ? 'True' : 'False');
    }

    /**
     * Gets whether Basic Auth can be used by the user.
     */
    private function getBasicAuthEnabled($userId) {
      return get_user_meta($userId, "{$this->getName()}_enabled", true) == 'True' ? true : false;
    }
  }
}

/**
 * Starts BasicAuth.
 */
function BasicAuth() {
  return BasicAuth::get_instance();
}

BasicAuth();

?>
