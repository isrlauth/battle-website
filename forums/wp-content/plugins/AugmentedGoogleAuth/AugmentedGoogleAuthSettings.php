<?php
/**
 * Class for showing settings.
 */

require_once 'AugmentedGoogleAuth.php';

if (!class_exists('AugmentedGoogleAuthSettings')) {
  class AugmentedGoogleAuthSettings {

    /**
     * Singleton code.
     */
    private static $instance = null;

    public static function getInstance() {
      if (null == self::$instance) {
        self::$instance = new self;
      }
      return self::$instance;
    }


    /**
     * Constructer.
     */
    function __construct() {
      add_action('admin_menu', array($this, 'adminMenuHook'));
      add_filter('plugin_action_links_' . plugin_basename(__DIR__ . '/AugmentedGoogleAuth.php'),
          array($this, 'settingsPageLink'));
    }

    /**
     * Gets a name that identifies this plugin. Matches GoogleAuth::getName().
     */
    private function getName() {
      return "AugmentedGoogleAuth";
    }

    /**
     * Adds the option page.
     */
    public function adminMenuHook() {
      add_options_page(__('Augmented Google Auth Settings'), __('Augmented Google Auth'), 'manage_options',
          'augmented-google-auth-settings', array($this, 'displaySettings'));
    }

    /**
     * Displays a setting link on the page.
     */
    public function settingsPageLink($links) {
      $settingsUrl = admin_url('options-general.php?page=augmented-google-auth-settings');
      array_unshift($links, "<a href=\"{$settingsUrl}\">Settings</a>");
      return $links;
    }

    /**
     * Displays the setting page.
     */
    public function displaySettings() {
      if (!current_user_can('manage_options')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
      }

      // Updates if this is a POST.
      $displayUpdated = false;
      if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['appId'])) {
          update_option("{$this->getName()}_appId", $_POST['appId']);
        }
        if (isset($_POST['secret'])) {
          update_option("{$this->getName()}_secret", $_POST['secret']);
        }
        update_option("{$this->getName()}_authEnabled", isset($_POST['authEnabled']) ? 'True' : 'False');
        update_option("{$this->getName()}_registerEnabled", isset($_POST['registerEnabled']) ? 'True' : 'False');
        $displayUpdated = true;
      }

      // Get the current values.
      $appId = get_option("{$this->getName()}_appId", '');
      $secret = get_option("{$this->getName()}_secret", '');
      $authEnabled = get_option("{$this->getName()}_authEnabled", 'True') === 'True' ? true : false;
      $registerEnabled = get_option("{$this->getName()}_registerEnabled", 'True') === 'True' ? true : false;
      $submitUrl = admin_url('options-general.php?page=augmented-google-auth-settings');

      ?>
      <div class="wrap">
        <h2><?php _e('Augmented Google Auth Settings'); ?></h2>

        <?php if ($displayUpdated) : ?>
          <div id="message" class="updated">
            <p><strong>
                <?php _e('Profile updated.'); ?>
              </strong></p>
          </div>
        <?php endif; ?>

        <form id="AugmentedGoogleAuthSettings" name="AugmentedGoogleAuthSettings" method="post"
              action="<?php echo esc_url($submitUrl); ?>">

          <table class="form-table">
            <tr>
              <th scope="row"><?php _e('Augmented Google Client ID'); ?></th>
              <td>
                <input type="text" name="appId" value="<?php echo $appId; ?>" size="80"/>
              </td>
            </tr>

            <tr>
              <th scope="row"><?php _e('Augmented Google Client Secret'); ?></th>
              <td>
                <input type="text" name="secret" value="<?php echo $secret; ?>" size=40"/>
              </td>
            </tr>

            <tr>
              <th scope="row"><?php _e("Show default authentication prompt") ?></th>
              <td>
                <input name="authEnabled" id="authEnabled" type="checkbox"
                    <?php checked($authEnabled); ?> />
              </td>
            </tr>
            <tr>
              <th scope="row"><?php _e("Show default register prompt") ?></th>
              <td>
                <input name="registerEnabled" id="registerEnabled" type="checkbox"
                    <?php checked($registerEnabled); ?> />
              </td>
            </tr>

          </table>

          <?php submit_button(__('Save Changes')) ?>
        </form>
      </div>
    <?php
    }
  }
}

/**
 * Starts BasicAuth.
 */
function AugmentedGoogleAuthSettings() {
  return AugmentedGoogleAuthSettings::getInstance();
}

AugmentedGoogleAuthSettings();

?>

