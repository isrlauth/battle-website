<?php

/**
 * Plugin Name: Google Auth
 * Plugin URI: http://isrlresearch.byu.edu/
 * Description: A plugin that allows loging in with Google.
 * Version: 0.1
 * Author: Scott Ruoti
 * Author URI: http://isrl.byu.edu.com/
 * License: None
 * Network: true
 * Text Domain: google-auth
 * Domain Path: /lang
 */

// Options page.
require_once 'GoogleAuthSettings.php';

if (!class_exists('GoogleAuth')) {
  require_once 'core/AuthCore.php';

  class GoogleAuth extends AuthCore {

    /**
     * Singleton code.
     */
    private static $instance = null;

    public static function getInstance() {
      if (null == self::$instance) {
        self::$instance = new self;
      }
      return self::$instance;
    }

    /**
     * Reference to the google client.
     */
    private $googleClient = null;

    /**
     * Reference to the oauth client.
     */
    private $oauthClient = null;

    /**
     * Constructer.
     */
    function __construct() {
      parent::__construct();
    }

    /**
     * Gets the google client for this instance.
     */
    protected function getGoogleClient() {
      if (!class_exists('Google_Client')) {
        $originalPath = set_include_path(get_include_path() . PATH_SEPARATOR .
            plugin_dir_path(__FILE__) . "/google-php-sdk");
        require_once 'google-php-sdk/Google/Client.php';
        set_include_path($originalPath);
      }

      if (null == $this->googleClient) {
        $this->googleClient = new Google_Client();
        $this->googleClient->setClientId(get_option("{$this->getName()}_appId", ''));
        $this->googleClient->setClientSecret(get_option("{$this->getName()}_secret", ''));
        $this->googleClient->setScopes(array('openid', 'email'));
      }
      return $this->googleClient;
    }

    /**
     * Gets the google client for this instance.
     */
    protected function getOAuthClient() {
      if (!class_exists('Google_Service_Oauth2')) {
        $originalPath = set_include_path(get_include_path() . PATH_SEPARATOR .
            plugin_dir_path(__FILE__) . "/google-php-sdk");
        require_once 'google-php-sdk/Google/Service/Oauth2.php';
        set_include_path($originalPath);
      }

      if (null == $this->oauthClient) {
        $this->oauthClient = new Google_Service_Oauth2($this->getGoogleClient());
      }
      return $this->oauthClient;
    }


    /**
     * Gets a verified user. Used to process an authentication response.
     */
    protected function getVerifiedUser($redirectUri) {
      // Check for errors.
      if (!isset($_REQUEST['code']) || !isset($_REQUEST['state'])) {
        return new WP_Error('google', __('<strong>ERROR</strong>: Unable to verify account with Google.'));
      }
      if (isset($_REQUEST['error'])) {
        switch ($_REQUEST['error']) {
          case 'access_denied':
            return new WP_Error('google',
                '<strong>ERROR</strong>: You did not grant access to the website to log you in using Google.');
            break;
          default:
            return new WP_Error('google', '<strong>ERROR</strong>: ' . htmlentities2($_REQUEST['error']));
            break;
        }
      }

      // Verify the state token.
      $stateVars = explode('|', urldecode($_REQUEST['state']));
      if (count($stateVars) != 2) {
        return new WP_Error('google', __('<strong>ERROR</strong>: Unable to verify account with Google.'));
      }
      $wpSession = WP_Session::get_instance();
      $stateToken = $wpSession["{$this->getName()}_nonce"];
      if (!wp_verify_nonce($stateVars[0], $stateToken)) {
        return new WP_Error('google', __('<strong>ERROR</strong>: Unable to verify account with Google.'));
      }
      // Handle the redirect, as it can't be part of the redirect_uri given to Google.
      if (strlen($stateVars[1]) > 0) {
        $_GET['redirect_to'] = $_POST['redirect_to'] = $_REQUEST['redirect_to'] = $stateVars[1];
      }

      // Gets the registration code from the URL.
      $google = $this->getGoogleClient();
      $google->setRedirectUri($redirectUri);
      $oauth = $this->getOAuthClient();
      try {
        $google->authenticate($_GET['code']);
        $userInfo = $oauth->userinfo->get();
        if (!($userInfo && is_object($userInfo) && property_exists($userInfo, 'id') &&
            property_exists($userInfo, 'email') && property_exists($userInfo, 'verifiedEmail'))
        ) {
          return new WP_Error('google', __('<strong>ERROR</strong>: Unable to verify account with Google.'));
        }
        if (!$userInfo->verifiedEmail) {
          return new WP_Error('google', __('<strong>ERROR</strong>: This Google account is not associated' .
              'with a verified email address.'));
        }
        return array($userInfo->id, $userInfo->email);
      } catch (Google_Auth_Exception $e) {
        return new WP_Error('google', __('<strong>ERROR</strong>: Unable to verify account with Google.'));
      }
    }

    /**
     * Gets the auth URI to use.
     */
    protected function redirectToAuthUrl($redirectUri) {
      $google = $this->getGoogleClient();
      $google->setRedirectUri($redirectUri);

      // Setup a state check to use. Also encode the redirect uri if we have one.
      $wpSession = WP_Session::get_instance();
      $stateToken = $this->getName() . '-' . md5(rand());
      $wpSession["{$this->getName()}_nonce"] = $stateToken;

      $redirectTo = (isset($_REQUEST['redirect_to']) && $_REQUEST['redirect_to'] != '') ?
          $_REQUEST['redirect_to'] : '';
      $google->setState(urlencode(wp_create_nonce($stateToken) . '|' . $redirectTo));
      wp_redirect($google->createAuthUrl());
      exit;
    }


    /**
     * Gets a name that identifies this plugin. Should be unique from all other
     * authentication plugins. Used in various functions to identify this plugin.
     * It is also used as a query variable and should be so compatible.
     */
    protected function getName() {
      return "GoogleAuth";
    }

    /**
     * Gets the content to be put on the login page.
     */
    protected function getLoginFormContent() {
      ob_start();
      ?>

      <div id="GoogleAuthDiv">
        <a id="GoogleAuth_url" href="#" rel="nofollow">
          <img style="display: block; margin: auto"
               src="<?php echo esc_url(plugins_url('img/GoogleLogin.png', __FILE__)) ?>"/>
        </a>
      </div>
      <form name="GoogleAuthForm" id="GoogleAuthForm" style="display: none;" method="post"
            action="<?php echo esc_url($this->getAuthenticateUrl()); ?>">
      </form>

      <script>
        // Clicking the link submits the form.
        jQuery('div#GoogleAuthDiv a#GoogleAuth_url').on('click', function () {
          jQuery('form#GoogleAuthForm').trigger('submit');
          return false;
        });

        // Grab redirect_to from the main form.
        jQuery(document).ready(function () {
          jQuery('form#loginform input[name="redirect_to"]').clone().appendTo(jQuery('form#GoogleAuthForm'));
        });
      </script>

      <?php
      return ob_get_clean();
    }

    /**
     * Authenticates the user.
     */
    protected function authenticateUser($user, $username = null, $password = null) {
      // Respond differently to the login request and the google redirect.
      $redirectUri = add_query_arg("{$this->getName()}", 'glogin', $this->getAuthenticateUrl());
      if ($_REQUEST[$this->getName()] == 'glogin') {
        $gInfo = $this->getVerifiedUser($redirectUri);
        if (is_wp_error($gInfo)) {
          return $gInfo;
        }
        $gUser = $gInfo[0];

        $user = $this->getUserByGoogleId($gUser);
        if (!$user) {
          return new WP_Error('google',
              __('<strong>ERROR</strong>: No user is registered with the selected Google account.'));
        }
        return $user;
      } else {
        $this->redirectToAuthUrl($redirectUri);
      }
    }

    /**
     * Gets the content to be put on the registration page.
     */
    protected function getRegisterFormContent() {
      // Otherwise return the login form.
      if (isset($_POST['user_login'])) {
        $user_login = $_POST['user_login'];
      }

      ob_start();
      ?>

      <form name="GoogleAuthForm" id="GoogleAuthForm" action="<?php echo esc_url($this->getRegisterUrl()); ?>"
            method="post">
        <p>
          <label for="user_login"><?php _e('Username') ?><br/></label>
          <input type="text" name="user_login" id="google_auth_user_login" class="input"
                 value="<?php echo esc_attr(wp_unslash($user_login)); ?>" size="20"/>
        </p>
        <a id="GoogleAuth_url" href="#" rel="nofollow">
          <img style="display: block; margin: auto"
               src="<?php echo esc_url(plugins_url('img/GoogleRegister.png', __FILE__)) ?>"/>
        </a>
      </form>

      <script>
        // Clicking the link submits the form.
        jQuery('form#GoogleAuthForm a#GoogleAuth_url').on('click', function () {
          jQuery('form#GoogleAuthForm').trigger('submit');
          return false;
        });

        // Grab redirect_to from the main form.
        jQuery(document).ready(function () {
          jQuery('form#registerform input[name="redirect_to"]').clone().appendTo(jQuery('form#GoogleAuthForm'));
        });
      </script>

      <?php
      return ob_get_clean() . $this->transformRegisterGetToPost();
    }

    /**
     * Gets HTML that will transform the current get request to a post request if neccessary for
     * registering the user. This is done since register_new_user() is only called for POST
     * requests.
     */
    protected function transformRegisterGetToPost() {
      if ($_SERVER['REQUEST_METHOD'] === 'GET' &&
          isset($_REQUEST[$this->getName()]) && $_REQUEST[$this->getName()] == 'gregister'
      ) {
        $wpSession = WP_Session::get_instance();
        $username = isset($wpSession["{$this->getName()}_username"]) ? $wpSession["{$this->getName()}_username"] : '';

        ob_start();
        ?>

        <form name="GoogleSubmitForm" id="GoogleSubmitForm" method="post" style="display: none;"
              action="<?php echo esc_url($_SERVER['REQUEST_URI']); ?>">
          <input type="text" name="user_login" value="<?php echo esc_attr($username); ?>"/>
        </form>

        <?php $this->showAuthCoreDialog('Creating Account', plugins_url('img/loader.gif', __FILE__)); ?>

        <script>
          // Immediately submit the form.
          jQuery(document).ready(function () {
            jQuery('form#GoogleSubmitForm').trigger('submit');
          });
        </script>

        <?php
        return ob_get_clean();
      }
      return '';
    }

    /**
     * Authenticates the user with this authentication method.
     */
    protected function registerUser($username, $usermail, $errors) {
      // If there is a problem with the username we cannot register it.
      $newErrors = new WP_Error();
      foreach (array('empty_username', 'invalid_username', 'username_exists') as $code) {
        $this->copyErrors($errors, $newErrors, $code);
      }
      if ($newErrors->get_error_code() != '') {
        return $newErrors;
      }

      $redirectUri = add_query_arg("{$this->getName()}", 'gregister', $this->getRegisterUrl());
      if ($_REQUEST[$this->getName()] == 'gregister') {
        $gInfo = $this->getVerifiedUser($redirectUri);
        if (is_wp_error($gInfo)) {
          return $gInfo;
        }
        $gUser = $gInfo[0];

        $user = $this->getUserByGoogleId($gUser);
        if ($user) {
          return new WP_Error('google',
              __('<strong>ERROR</strong>: A user is already associated with the selected Google account.'));
        }

        $usermail = $gInfo[1];
        $userpass = wp_generate_password(12, false);
        $userId = wp_create_user($username, $userpass, $usermail);
        if (is_wp_error($userId)) {
          if ($userId->get_error_code() == 'existing_user_email') {
            return new WP_Error('google', '<strong>ERROR</strong>: The email associated with the ' .
                'selected Google account is already in use on this site.');
          } else {
            return $userId;
          }
        }
        if (!$userId) {
          return new WP_Error('registerfail',
              sprintf(__('<strong>ERROR</strong>: Couldn\'t register you&hellip; please contact the ' .
                  '<a href="mailto:%s">webmaster</a> !'), get_option('admin_email')));
        }
        $this->setGoogleId($userId, $gUser);
        return $userId;
      } else {
        // Store the username. We do this since it cannot have come through post on the redirect.
        $wpSession = WP_Session::get_instance();
        $wpSession["{$this->getName()}_username"] = $username;

        $this->redirectToAuthUrl($redirectUri);
      }
    }

    /**
     * Gets the content to be put on the user's profile page.
     */
    protected function getProfileContent($user) {
      // If the user can't login normally, remove the options to change the linking of the account.
      if ($this->removeDefaultLogin()) {
        return '';
      }

      $gUser = $this->getGoogleId($user->ID);
      $innerText = '';

      // Unlink the account.
      if ($gUser) {
        ob_start();
        ?>
        <a id="GoogleAuth_url" href="#" rel="nofollow">
          <img style="display: block;"
               src="<?php echo esc_url(plugins_url('img/GoogleUnlink.png', __FILE__)) ?>"/>
        </a>
        <br/>
        <span class="description">
        <?php _e('Unlinking your account will remove the ability to use Google to log in to this site.'); ?>
      </span>

        <script>
          var button = jQuery('form a#GoogleAuth_url');
          button.on('click', function () {
            jQuery('form input[type="hidden"]#googleAction').val('unlink');
            jQuery('form input[type="submit"]#submit').trigger('click');
            return false;
          });
        </script>
      <?php
      } else {
        // Link the account.
        ob_start();
        ?>
        <a id="GoogleAuth_url" href="#" rel="nofollow">
          <img style="display: block;"
               src="<?php echo esc_url(plugins_url('img/GoogleLink.png', __FILE__)) ?>"/>
        </a>
        <br/>
        <span class="description">
        <?php _e('Linking your account will allow you to use Google to log in to this site.'); ?><br>
        <?php _e('Linking your account will log you out. Log back in using your new credentials.'); ?>
      </span>

        <script>
          var button = jQuery('form a#GoogleAuth_url');
          button.on('click', function () {
            jQuery('form input[type="hidden"]#googleAction').val('link');
            jQuery('form input[type="submit"]#submit').trigger('click');
            return false;
          });
        </script>
      <?php
      }
      $innerText = ob_get_clean();

      ob_start();
      ?>

      <h3>Google Authentication</h3>
      <table class="form-table">
        <tbody>
        <tr>
          <th scope="row"><?php _e("Google Account") ?></th>
          <td>
            <input type='hidden' id='googleAction' name='googleAction' value=''/>
            <?php echo $innerText; ?>
            <br/>
          </td>
        </tr>
        </tbody>
      </table>

      <?php
      return ob_get_clean() . $this->transformLinkGetToPost();
    }

    /**
     * Gets HTML that will transform the current get request to a post request if neccessary for
     * registering the user. This is done since register_new_user() is only called for POST
     * requests.
     */
    protected function transformLinkGetToPost() {
      if ($_SERVER['REQUEST_METHOD'] === 'GET' &&
          isset($_REQUEST[$this->getName()]) && $_REQUEST[$this->getName()] == 'link'
      ) {
        ob_start();
        ?>

        <?php $this->showAuthCoreDialog('Updating account', plugins_url('img/loader.gif', __FILE__)); ?>

        <script>
          // Immediately submit the form.
          jQuery(document).ready(function () {
            var form = jQuery('form');
            console.log(form.attr('action'));
            form.attr('action', '<?php echo $_SERVER['REQUEST_URI']; ?>');
            jQuery('input[type="submit"]#submit', form).trigger('click');
          });
        </script>

        <?php
        return ob_get_clean();
      }
      return '';
    }

    /**
     * Updates the user's personal options.
     */
    protected function updatePersonalOptions($userId, $errors) {
      // If the user can't login normally, remove the options to change the linking of the account.
      if ($this->removeDefaultLogin()) {
        return;
      }

      if (current_user_can('edit_user', $userId) && isset($_POST['googleAction'])) {
        $googleAction = $_POST['googleAction'];
        $redirectUri = add_query_arg("{$this->getName()}", 'link', self_admin_url('profile.php'));
        if ($googleAction == 'unlink') {
          $this->setGoogleId($userId, '');
        } elseif ($googleAction == 'link') {
          $this->redirectToAuthUrl($redirectUri);
        } elseif ($_REQUEST[$this->getName()] == 'link') {
          $gInfo = $this->getVerifiedUser($redirectUri);
          if (is_wp_error($gInfo)) {
            $this->copyErrors($gInfo, $errors, 'google');
            return;
          }
          $gUser = $gInfo[0];

          // Ensure only one user has this google ID.
          $user = $this->getUserByGoogleId($gUser);
          if ($user) {
            $errors->add('google',
                __('<strong>ERROR</strong>: A user is already associated with the selected Google account.'));
            return;
          }
          $password = "defaultpassword";

          $this->setGoogleId($userId, $gUser);
          wp_set_password($password,$userId);
        }
      }
    }

    /**
     * Sets an option indicating the given user's google id.
     */
    private function setGoogleId($userId, $googleId) {
      update_user_meta($userId, "{$this->getName()}_id", $googleId);
    }

    /**
     * Gets an option setting the given user's google id.
     */
    private function getGoogleId($userId) {
      return intval(get_user_meta($userId, "{$this->getName()}_id", true));
    }

    /**
     * Gets the user with the given google Id.
     */
    private function getUserByGoogleId($googleId) {
      $users = get_users(array('meta_key' => "{$this->getName()}_id", 'meta_value' => $googleId));
      if (empty($users)) {
        return null;
      } else {
        return $users[0];
      }
    }

  }
}

/**
 * Starts BasicAuth.
 */
function GoogleAuth() {
  return GoogleAuth::getInstance();
}

GoogleAuth();

?>
