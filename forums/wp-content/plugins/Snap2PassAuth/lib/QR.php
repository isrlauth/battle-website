<?php
/**
 * PHP script that generates QR codes.
 */
include('qrlib/qrlib.php');

QRcode::png(base64_encode(base64_decode($_GET['data'])), false, QR_ECLEVEL_H, 4);
?>