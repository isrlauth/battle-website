<?php

/**
 * Plugin Name: Snap2Pass Auth
 * Plugin URI: http://isrlresearch.byu.edu/
 * Description: A plugin that allows logging in with Snap2Pass.
 * Version: 0.1
 * Author: Scott Ruoti
 * Author URI: http://isrl.byu.edu.com/
 * License: None
 * Network: true
 * Text Domain: snap2pass-auth
 * Domain Path: /lang
 */

// Options page.
require_once 'Snap2PassAuthSettings.php';

if (!class_exists('Snap2PassAuth')) {
  require_once 'core/AuthCore.php';

  class Snap2PassAuth extends AuthCore {

    /**
     * Singleton code.
     */
    private static $instance = null;

    public static function getInstance() {
      if (null == self::$instance) {
        self::$instance = new self;
      }
      return self::$instance;
    }

    /**
     * Constructer.
     */
    function __construct() {
      parent::__construct();
      add_action('parse_request', array($this, 'ajaxCheckTransaction'), 1);
      add_action('parse_request', array($this, 'registerTransaction'), 1);
    }

    /**
     * Gets a name that identifies this plugin. Should be unique from all other
     * authentication plugins. Used in various functions to identify this plugin.
     * It is also used as a query variable and should be so compatible.
     */
    protected function getName() {
      return "Snap2PassAuth";
    }

    /**
     * Show the Snap2Pass image.
     */
    protected function getLoginFormContent() {
      // Generate a transaction number that will handle the received authentication params.
      $wpSession = WP_Session::get_instance();
      $transactionId = uniqid('snap2pass');
      $wpSession["{$this->getName()}_transactionId"] = $transactionId;

      // URL to QR code.
      $url = add_query_arg('data', base64_encode("{$transactionId}\n{$this->getSnap2PassUrl()}"),
          plugins_url('lib/QR.php', __FILE__));

      ob_start();
      ?>
      <form>
        <h3 align="center" style="margin-bottom: 15px;">
          Take a picture of this image with the Snap2Pass application to log in.
        </h3>
        <img src="<?php echo esc_url($url); ?>" style="display:block; margin:auto; border: solid 5px"/>
      </form>

      <script>
        window.setInterval(function () {
          jQuery.ajax('/?isLoggedIn')
              .done(function (data) {
                if (data && data.loggedIn)
                  window.location = '<?php echo site_url('/', 'https'); ?>';
              });
        }, 1000);
      </script>
      <?php
      return ob_get_clean();
    }

    /**
     * Sets user credentials based on what is given by Snap2Pass.
     */
    protected function authenticateUser($user, $username = null, $password = null) {
    }

    /**
     * Checks if the transaction was set by the mobile device.
     */
    public function ajaxCheckTransaction() {
      if (isset($_GET['isLoggedIn'])) {
        $wpSession = WP_Session::get_instance();
        $transactionId = $wpSession["{$this->getName()}_transactionId"];
        //error_log($transactionId);
        $transient = get_transient($transactionId);
        if ($transient) {
          $info = maybe_unserialize($transient);
          wp_signon(array('user_login' => $info['user'], 'user_password' => $info['password']), true);
          delete_transient($transactionId);
        }
      }
    }

    /**
     * Register the transaction from the phone.
     */
    public function registerTransaction() {
      if (isset($_GET['Snap2PassSubmit'])) {
        if ($_POST["transactionId"]) {
          set_transient($_POST["transactionId"],
              maybe_serialize(array('user' => $_POST['user'], 'password' => $_POST['pass'])),
              30 * MINUTE_IN_SECONDS);
          $this->sendCorsResponse("{success: true}");
        }
      }
    }

    /**
     * Show the register form.
     */
    protected function getRegisterFormContent() {
      $user_login = $_POST['user_login'];
      $user_email = $_POST['user_email'];

      ob_start();
      ?>
      <form name="Snap2PassRegister" id="Snap2PassRegister" action="<?php echo esc_url($this->getRegisterUrl()); ?>"
            method="post">
        <p>
          <label for="user_login"><?php _e('Username') ?><br/>
            <input type="text" name="user_login" id="user_login" class="input"
                   value="<?php echo esc_attr(wp_unslash($user_login)); ?>" size="20"/></label>
        </p>

        <p>
          <label for="user_email"><?php _e('E-mail') ?><br/>
            <input type="text" name="user_email" id="user_email" class="input"
                   value="<?php echo esc_attr(wp_unslash($user_email)); ?>" size="25"/></label>
        </p>

        <p id="reg_passmail"><?php _e('A Snap2Pass QR code will be mailed to you.') ?></p>
        <br class="clear"/>

        <p class="submit"><input type="submit" name="wp-submit" id="wp-submit"
                                 class="button button-primary button-large"
                                 value="<?php esc_attr_e('Register'); ?>"/></p>
      </form>
      <?php
      return ob_get_clean();
    }

    /**
     * Authenticates the user with this authentication method.
     */
    protected function registerUser($username, $usermail, $errors) {
      $errors = apply_filters('registration_errors', $errors, $username, $usermail);

      if ($errors->get_error_code()) {
        return $errors;
      }

      $user_pass = wp_generate_password(12, false);
      $user_id = wp_create_user($username, $user_pass, $usermail);
      if (!$user_id || is_wp_error($user_id)) {
        $errors->add('registerfail',
            sprintf(__('<strong>ERROR</strong>: Couldn&#8217;t register you&hellip; please contact the <a href="mailto:%s">webmaster</a> !'),
                get_option('admin_email')));
        return $errors;
      }
      add_user_meta($user_id, 'user_password', $user_pass, true);

      // URL to QR code.
      $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
      $url = add_query_arg('data', base64_encode("{$username}\n{$user_pass}\n{$this->getSnap2PassUrl()}\n{$blogname}"),
          plugins_url('lib/QR.php', __FILE__));
      $img = "<img src='{$url}' style='display:block; border: solid 5px' />";

      // Send e-mail with QR code for Snap2Pass.
      $message = __('To finish registering your account please take a picture of the below QR code with Snap2Pass. ' .
              'You can then return to the website and log in.') . "<br /><br />";
      $message .= $img;
      add_filter('wp_mail_content_type', function () { return "text/html"; });
      wp_mail($usermail, sprintf(__('[%s] Verify Registration (%s)'), $blogname, date('Y/m/d g:i:sA')), $message);

      return $user_id;
    }

    /**
     * Called when a user is being created. This method will hijack user creation if successful,
     * otherwise it will store errors into the session that will be displayed by
     * filterRegistrationErrors.
     */
    public function registerUserPost($username, $usermail, $errors) {
      if ($this->isEnabled() && isset($_REQUEST[$this->getName()])) {
        $wpSession = WP_Session::get_instance();
        unset($wpSession["{$this->getName()}_registrationError"]);
        $userId = $this->registerUser($username, $usermail, $errors);

        if (is_wp_error($userId)) {
          $wpSession["{$this->getName()}_registrationError"] = $userId;
        } else {
          wp_safe_redirect(add_query_arg('registered', $this->getName(), site_url('wp-login.php')));
          exit();
        }
      }
    }

    /**
     * Display any custom login errors we want.
     *
     * For now we just add a message, which is implemented as a WP_Error, that tells users when
     * they have successfully registered.
     */
    public function displayLoginErrors($errors) {
      if ($this->isEnabled() && isset($_REQUEST['registered']) &&
          $_REQUEST['registered'] == $this->getName()
      ) {
        $errors->add('registered', __('Please check your e-mail to complete registration. ' .
            'You can then use the QR code below to log into the website.'), 'message');
      }
      return $errors;
    }

    /**
     * Adds a short method describing how to use SAW.
     */
    protected function getProfileContent($user) {
      $username = $user->user_login;
      $user_pass = get_user_meta($user->ID, "user_password", true);

      $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
      $url = add_query_arg('data', base64_encode("{$username}\n{$user_pass}\n{$this->getSnap2PassUrl()}\n{$blogname}"),
          plugins_url('lib/QR.php', __FILE__));
      $img = "<img src='{$url}' style='display:block; border: solid 5px' />";

      ob_start();
      ?>

      <label for="email"><h2 style="color: red;">Snap2Pass Authentication</h2></label>
      <table class="form-table">
        <tbody>
        <tr>
          <th scope="row"><?php _e("Snap2Pass") ?></th>
          <td>
            To register and use this account with Snap2Pass, please take a picture of the following QR code with the
            Snap2Pass application. <br/><br/>
            <?php echo $img; ?>
            <br/>
          </td>
        </tr>
        </tbody>
      </table>

      <?php
      return ob_get_clean();
    }

    /**
     * Not used in Saw.
     */
    protected function updatePersonalOptions($userId, $errors) { }

    /**
     * Get the url for posting snap2pass responses.
     */
    protected function getSnap2PassUrl() {
      return add_query_arg('Snap2PassSubmit', '', site_url('/', 'https'));
    }

  }
}

/**
 * Starts Snap2PassAuth.
 */
function Snap2PassAuth() {
  return Snap2PassAuth::getInstance();
}

Snap2PassAuth();

?>
