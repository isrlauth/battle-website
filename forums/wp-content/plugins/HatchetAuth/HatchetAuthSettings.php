<?php
/**
 * Class for showing settings.
 */

if (!class_exists('HatchetAuthSettings')) {
  class HatchetAuthSettings {

    /**
     * Singleton code.
     */
    private static $instance = null;

    public static function getInstance() {
      if (null == self::$instance) {
        self::$instance = new self;
      }
      return self::$instance;
    }


    /**
     * Constructer.
     */
    function __construct() {
      add_action('admin_menu', array($this, 'adminMenuHook'));
      add_filter('plugin_action_links_' . plugin_basename(__DIR__ . '/HatchetAuth.php'),
          array($this, 'settingsPageLink'));
    }

    /**
     * Gets a name that identifies this plugin. Matches HatchetAuth::getName().
     */
    private function getName() {
      return "HatchetAuth";
    }

    /**
     * Adds the option page.
     */
    public function adminMenuHook() {
      add_options_page(__('Hatchet Auth Settings'), __('Hatchet Auth'), 'manage_options',
          'hatchet-auth-settings', array($this, 'displaySettings'));
    }

    /**
     * Displays a setting link on the page.
     */
    public function settingsPageLink($links) {
      $settingsUrl = admin_url('options-general.php?page=hatchet-auth-settings');
      array_unshift($links, "<a href=\"{$settingsUrl}\">Settings</a>");
      return $links;
    }

    /**
     * Displays the setting page.
     */
    public function displaySettings() {
      if (!current_user_can('manage_options')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
      }

      // Updates if this is a POST.
      $displayUpdated = false;
      if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        update_option("{$this->getName()}_authEnabled", isset($_POST['authEnabled']) ? 'True' : 'False');
        update_option("{$this->getName()}_registerEnabled", isset($_POST['registerEnabled']) ? 'True' : 'False');
        $displayUpdated = true;
      }

      // Get the current values.
      $authEnabled = get_option("{$this->getName()}_authEnabled", 'True') === 'True' ? true : false;
      $registerEnabled = get_option("{$this->getName()}_registerEnabled", 'True') === 'True' ? true : false;
      $submitUrl = admin_url('options-general.php?page=hatchet-auth-settings');
      ?>

      <div class="wrap">
        <h2><?php _e('Hatchet Auth Settings'); ?></h2>

        <?php if ($displayUpdated) : ?>
          <div id="message" class="updated">
            <p><strong>
                <?php _e('Profile updated.'); ?>
              </strong></p>
          </div>
        <?php endif; ?>

        <form id="HatchetAuthSettings" name="HatchetAuthSettings" method="post"
              action="<?php echo esc_url($submitUrl); ?>">

          <table class="form-table">
            <tr>
              <th scope="row"><?php _e("Show default authentication prompt") ?></th>
              <td>
                <input name="authEnabled" id="authEnabled" type="checkbox"
                    <?php checked($authEnabled); ?> />
              </td>
            </tr>
            <tr>
              <th scope="row"><?php _e("Show default register prompt") ?></th>
              <td>
                <input name="registerEnabled" id="registerEnabled" type="checkbox"
                    <?php checked($registerEnabled); ?> />
              </td>
            </tr>
          </table>

          <?php submit_button(__('Save Changes')) ?>
        </form>
      </div>
    <?php
    }
  }
}

/**
 * Starts BasicAuth.
 */
function HatchetAuthSettings() {
  return HatchetAuthSettings::getInstance();
}

HatchetAuthSettings();

?>

