<?php

/**
 * Plugin Name: Saw Auth
 * Plugin URI: http://isrlresearch.byu.edu/
 * Description: A plugin that allows loging in with Saw.
 * Version: 0.1
 * Author: Scott Ruoti
 * Author URI: http://isrl.byu.edu.com/
 * License: None
 * Network: true
 * Text Domain: saw-auth
 * Domain Path: /lang
 */

// Options page.
require_once 'SawAuthSettings.php';

if (!class_exists('SawAuth')) {
  require_once 'core/AuthCore.php';

  class SawAuth extends AuthCore {

    /**
     * Singleton code.
     */
    private static $instance = null;

    public static function getInstance() {
      if (null == self::$instance) {
        self::$instance = new self;
      }
      return self::$instance;
    }

    /**
     * Constructer.
     */
    function __construct() {
      parent::__construct();
      add_action('parse_request', array($this, 'closeWindow'), 1);
      add_filter('login_redirect', array($this, 'closeWindowFilter'));
    }

    /**
     * Gets a name that identifies this plugin. Should be unique from all other
     * authentication plugins. Used in various functions to identify this plugin.
     * It is also used as a query variable and should be so compatible.
     */
    protected function getName() {
      return "SawAuth";
    }

    /**
     * Gets a user verified by the passed in code. If no user has been verified return false.
     */
    protected function getVerifiedUser() {
      $wpSession = WP_Session::get_instance();

      // We log in if we have a cookie and the state URL. This is a less secure
      // approximation of SAW. Indistinguishable to end-users.
      if (isset($_REQUEST['SawToken'])) {
        if (isset($wpSession["{$this->getName()}_code"]) &&
            isset($wpSession["{$this->getName()}_email"]) &&
            $wpSession["{$this->getName()}_code"] == $_REQUEST['SawToken']
        ) {
          return $wpSession["{$this->getName()}_email"];
        }
        //error_log($wpSession["{$this->getName()}_code"]);
        //error_log($_REQUEST['SawToken']);
        return new WP_Error('saw',
            __('<strong>ERROR</strong>: There has been an error verifying your e-mail. Please try again.'));
      }

      return false;
    }

    /**
     * Cleans up the session object.
     */
    protected function cleanupSession() {
      $wpSession = WP_Session::get_instance();

      unset($wpSession["{$this->getName()}_link"]);
      unset($wpSession["{$this->getName()}_code"]);
      unset($wpSession["{$this->getName()}_login"]);
      unset($wpSession["{$this->getName()}_email"]);
    }

    /**
     * Sets up the authentication for the user.
     *
     * @param url Url to redirect to with the token.
     */
    public function setupAuthentication($url) {
      // Generate code and setup session.
      $email = $_REQUEST['user_email'];
      $wpSession = WP_Session::get_instance();
      $code = wp_generate_password(64, false);
      $link = add_query_arg('SawToken', $code, $url);
      $wpSession["{$this->getName()}_link"] = $link;
      $wpSession["{$this->getName()}_code"] = $code;
      $wpSession["{$this->getName()}_email"] = $email;

      if (!(isset($_REQUEST['saw_hatchet_plugin']) && $_REQUEST['saw_hatchet_plugin'] == $email)) {
        $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
        $message = __('To finish logging in click the link below:') . "<br /><br />";
        $message .= sprintf(
                __('<h2><a href="%s">Click this link to verify your account.</a></h2>'),
                add_query_arg('close', 'true', $link)) . "<br />";
        add_filter('wp_mail_content_type', function () { return "text/html"; });
        wp_mail($email, sprintf(__('[%s] Verify Login (%s)'), $blogname, date('Y/m/d g:i:sA')), $message);
      }
    }

    /**
     * Gets the content to be put on the login page.
     */
    protected function getLoginFormContent() {
      if (isset($_POST['user_email'])) {
        $user_email = $_POST['user_email'];
      }
      $wpSession = WP_Session::get_instance();

      // User needs to enter their code.
      if (isset($_REQUEST[$this->getName()])) {
        $email = $wpSession["{$this->getName()}_email"];
        $link = $wpSession["{$this->getName()}_link"];

        ob_start();
        ?>
        <input type="hidden" name="saw_hatchet_plugin_link" id="saw_plugin_link"
               value="<?php echo esc_url($link); ?>"/>
        <input type="hidden" name="saw_hatchet_plugin_email" id="saw_plugin_email"
               value="<?php echo esc_attr(wp_unslash($email)); ?>"/>

        <p id="nav" style="padding-left: 0px;">
          <a href="<?php echo wp_login_url(); ?>" title="Start Over.">← Start Over</a>
        </p>
        <?php
        $content = ob_get_clean();
      } else {
        if (isset($_REQUEST['user_email'])) {
          $user_email = $_REQUEST['user_email'];
        }

        ob_start();
        ?>
        <p>
          <label for="user_email"><?php _e('E-mail'); ?><br>
            <input type="text" name="user_email" id="saw_user_email" class="input"
                   value="<?php echo esc_attr($user_email); ?>" size="20"/>
          </label>
        </p>

        <a id="SawAuth_url" href="#" rel="nofollow">
          <img style="display: block; margin: auto"
               src="<?php echo esc_url(plugins_url('img/SawLogin.png', __FILE__)) ?>"/>
        </a>
        <?php
        $content = ob_get_clean();
      }

      ob_start();
      ?>
      <form name="SawAuthForm" id="SawAuthForm" style="padding-bottom: 26px;"
            action="<?php echo esc_url($this->getAuthenticateUrl()); ?>" method="post">
        <input type="hidden" name="saw_hatchet_plugin" id="saw_plugin" value="false"/>
        <?php echo $content; ?>
      </form>

      <script>
        window.setInterval(function () {
          jQuery.ajax('/?isLoggedIn')
              .done(function (data) {
                if (data && data.loggedIn)
                  window.location = '<?php echo site_url('/', 'https'); ?>';
              });
        }, 1000);

        // Clicking the link submits the form.
        jQuery('form#SawAuthForm a#SawAuth_url').on('click', function () {
          jQuery('form#SawAuthForm').trigger('submit');
          return false;
        });

        // Grab the redirect_to field.
        jQuery(document).ready(function () {
          jQuery('form#loginform input[name="redirect_to"]').clone().insertAfter(jQuery('#saw_plugin'));
          for (var i = 0; i <= 3; i++)
            window.setTimeout(function () {
              jQuery('#saw_user_email').focus();
            }, i * 100);
        });
      </script>
      <?php
      return ob_get_clean();
    }

    /**
     * Authenticates the user.
     */
    protected function authenticateUser($user, $username = null, $password = null) {
      // Send an authentication e-mail.
      if (isset($_REQUEST['user_email'])) {
        if (!is_email($_REQUEST['user_email'])) {
          unset($_REQUEST[$this->getName()]);
          return new WP_Error('saw',
              __('<strong>ERROR</strong>: Invalid e-mail address entered.'));
        }

        $this->setupAuthentication($this->getAuthenticateUrl());
        return new WP_Error('saw',
            __('<strong>SUCCESS</strong>: An email has been sent with a one use token to use to log in.'), 'message');
      } // Handle the OTP being submitted.
      else {
        if (isset($_REQUEST['SawToken'])) {
          $email = $this->getVerifiedUser();
          if (is_wp_error($email)) {
            return $email;
          } else {
            if ($email) {
              $this->cleanupSession();
              unset($_REQUEST[$this->getName()]);
              $user = get_user_by('email', $email);
              if (!$user) {
                return new WP_Error('saw', __('<strong>ERROR</strong>: No user with the given e-mail address.'));
              } else {
                if (is_wp_error($user)) {
                  return $user;
                } else {
                  return $user;
                }
              }
            }
          }
        }
      }

      // We should have returned something to the user at this point.
      unset($_REQUEST[$this->getName()]);
      $this->cleanupSession();
      return new WP_Error('SAW', __('<strong>ERROR</strong>: Unknown error, please try again.'));
    }

    /**
     * Gets the content to be put on the registration page.
     */
    protected function getRegisterFormContent() {
      $wpSession = WP_Session::get_instance();

      // User needs to enter their code.
      if (isset($_REQUEST[$this->getName()])) {
        $link = $wpSession["{$this->getName()}_link"];
        $login = $wpSession["{$this->getName()}_login"];
        $email = $wpSession["{$this->getName()}_email"];

        ob_start();
        ?>
        <input type="hidden" name="user_login" id="saw_user_login"
               value="<?php echo esc_attr(wp_unslash($login)); ?>"/>
        <input type="hidden" name="user_email" id="saw_user_email"
               value="<?php echo esc_attr(wp_unslash($email)); ?>"/>

        <input type="hidden" name="saw_hatchet_plugin_link" id="saw_plugin_link"
               value="<?php echo esc_url($link); ?>"/>
        <input type="hidden" name="saw_hatchet_plugin_email" id="saw_plugin_email"
               value="<?php echo esc_attr(wp_unslash($email)); ?>"/>

        <p id="nav" style="padding-left: 0px;">
          <a href="<?php echo wp_registration_url(); ?>" title="Start Over.">← Start Over</a>
        </p>
        <?php
        $content = ob_get_clean();
      } else {
        if (isset($_REQUEST['user_login'])) {
          $user_login = $_REQUEST['user_login'];
        }
        if (isset($_REQUEST['user_email'])) {
          $user_email = $_REQUEST['user_email'];
        }

        ob_start();
        ?>
        <p>
          <label for="user_login"><?php _e('Username') ?><br/></label>
          <input type="text" name="user_login" id="saw_user_login" class="input"
                 value="<?php echo esc_attr(wp_unslash($user_login)); ?>" size="20"/>
        </p>

        <p>
          <label for="user_email"><?php _e('E-mail') ?><br/></label>
          <input type="text" name="user_email" id="saw_user_email" class="input"
                 value="<?php echo esc_attr(wp_unslash($user_email)); ?>" size="20"/>
        </p>

        <a id="SawAuth_url" href="#" rel="nofollow">
          <img style="display: block; margin: auto"
               src="<?php echo esc_url(plugins_url('img/SawRegister.png', __FILE__)) ?>"/>
        </a>
        <?php
        $content = ob_get_clean();
      }

      ob_start();
      ?>
      <form name="SawAuthForm" id="SawAuthForm" style="padding-bottom: 26px;"
            action="<?php echo esc_url($this->getRegisterUrl()); ?>" method="post">
        <input type="hidden" name="saw_hatchet_plugin" id="saw_plugin" value="false"/>
        <?php echo $content; ?>
      </form>

      <script>
        window.setInterval(function () {
          jQuery.ajax('/?isLoggedIn')
              .done(function (data) {
                if (data && data.loggedIn)
                  window.location = '<?php echo site_url('/', 'https'); ?>';
              });
        }, 1000);

        // Clicking the link submits the form.
        jQuery('form#SawAuthForm a#SawAuth_url').on('click', function () {
          jQuery('form#SawAuthForm').trigger('submit');
          return false;
        });

        // Grab the redirect_to field.
        jQuery(document).ready(function () {
          jQuery('form#loginform input[name="redirect_to"]').clone().insertAfter(jQuery('#saw_plugin'));
          for (var i = 0; i <= 3; i++)
            window.setTimeout(function () {
              jQuery('#saw_user_login').focus();
            }, i * 100);
        });
      </script>
      <?php
      return ob_get_clean() . $this->transformRegisterGetToPost();
    }

    /**
     * Gets HTML that will transform the current get request to a post request if neccessary for
     * registering the user. This is done since register_new_user() is only called for POST
     * requests.
     */
    protected function transformRegisterGetToPost() {
      if ($_SERVER['REQUEST_METHOD'] === 'GET' &&
          isset($_REQUEST[$this->getName()]) && $_REQUEST[$this->getName()] == 'register'
      ) {

        $wpSession = WP_Session::get_instance();
        $login = $wpSession["{$this->getName()}_login"];
        $email = $wpSession["{$this->getName()}_email"];
        ob_start();
        ?>
        <form name="SawAuthForm" id="SawAuthForm_redirect" action="<?php echo esc_url($this->getRegisterUrl()); ?>"
              method="post">
          <input type="hidden" name="user_login" id="saw_user_login"
                 value="<?php echo esc_attr(wp_unslash($login)); ?>"/>
          <input type="hidden" name="user_email" id="saw_user_email"
                 value="<?php echo esc_attr(wp_unslash($email)); ?>"/>
          <input type="hidden" name="SawToken" id="saw_token"
                 value="<?php echo esc_attr(wp_unslash($_REQUEST['SawToken'])); ?>">
          <input type="hidden" name="close" id="saw_close"
                 value="<?php echo esc_attr(wp_unslash($_REQUEST['close'])); ?>">
        </form>

        <?php $this->showAuthCoreDialog('Creating Account', plugins_url('img/loader.gif', __FILE__)); ?>

        <script>
          // Immediately submit the form.
          jQuery(document).ready(function () {
            jQuery('form#SawAuthForm_redirect').trigger('submit');
          });
        </script>
        <?php
        return ob_get_clean();
      }
      return '';
    }


    /**
     * Authenticates the user with this authentication method.
     */
    protected function registerUser($username, $usermail, $errors) {
      if ($errors->get_error_code() != '') {
        unset($_REQUEST[$this->getName()]);
        return $errors;
      }

      // Send an auth:entication e-mail.
      if (!isset($_REQUEST['SawToken'])) {
        $wpSession = WP_Session::get_instance();
        $wpSession["{$this->getName()}_login"] = $username;
        $this->setupAuthentication($this->getRegisterUrl());
        return new WP_Error('saw',
            __('<strong>SUCCESS</strong>: An email has been sent with a one use token to verify your registration.'),
            'message');
      } // Handle the OTP being submitted.
      else {
        $email = $this->getVerifiedUser();
        if (is_wp_error($email)) {
          return $email;
        }

        $this->cleanupSession();
        unset($_REQUEST[$this->getName()]);

        if ($email !== $usermail) {
          return new WP_Error('saw',
              __('<strong>ERROR</strong>: An uknown error has occurred, please try again.'));
        }

        $userpass = wp_generate_password(12, false);
        $userId = wp_create_user($username, $userpass, $usermail);
        if (is_wp_error($userId)) {
          if ($userId->get_error_code() == 'existing_user_email') {
            return new WP_Error('saw',
                __('<strong>ERROR</strong>: A user is already associated with the selected e-mail.'));
          } else {
            return $userId;
          }
        } else {
          if (!$userId) {
            return new WP_Error('registerfail',
                sprintf(__('<strong>ERROR</strong>: Couldn\'t register you&hellip; please contact the ' .
                    '<a href="mailto:%s">webmaster</a> !'), get_option('admin_email')));
          }
        }

        if ($_REQUEST['close']) {
          $_POST['redirect_to'] = site_url('/?closeWindow=register', 'https');
        }
        return $userId;
      }
    }

    /**
     * Adds a short method describing how to use SAW.
     */
    protected function getProfileContent($user) {
      ob_start();
      ?>

      <label for="email"><h2 style="color: red;">Saw Authentication</h2></label>
      <table class="form-table">
        <tbody>
        <tr>
          <th scope="row"><label for="email"><?php _e("Saw Account") ?></label></th>
          <td>
            SAW will use your email address to authenticate you. Please ensure that your account's email address (set
            below) matches the email address you wish to use for authentication. Once finished click "Update Pofile" at
            the bottom of the page.
            <br/>
          </td>
        </tr>
        </tbody>
      </table>

      <script>
        jQuery(function () {
          jQuery('label[for="email"]').css('color', 'red').css('font-size', '18px');
        });
      </script>

      <?php
      return ob_get_clean();
    }

    /**
     * Not used in Saw.
     */
    protected function updatePersonalOptions($userId, $errors) { }

    /**
     * Closes the current window.
     */
    public function closeWindow() {
      if (isset($_GET['closeWindow'])) {
        $operation = $_GET['closeWindow'] == 'login' ? 'Login' : 'Registration';
        ?>
        <html>
        <head>
          <style type="text/css">
            body {
              width: 100%;
              height: 100%;
            }

            body div {
              margin: auto;
              width: 100%;
              height: 100%;
              text-align: center;
            }
          </style>
        </head>
        <body>
        <div>
          <h1><?php echo $operation; ?> Complete</h1>

          <h2>Please close this window.</h2>
        </div>
        </body>
        </html>
        <?php
        exit;
      }
    }

    /**
     * Filter to redirect users to the close page if necessary.
     */
    public function closeWindowFilter($url) {
      if (isset($_REQUEST['close'])) {
        return site_url('/?closeWindow=login', 'https');
      } else return $url;
    }

  }
}

/**
 * Starts SawAuth.
 */
function SawAuth() {
  return SawAuth::getInstance();
}

SawAuth();

?>
