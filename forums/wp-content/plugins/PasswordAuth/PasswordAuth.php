<?php

/**
 * Plugin Name: Password Auth
 * Plugin URI: http://isrlresearch.byu.edu/
 * Description: The standard password login.
 * Version: 0.1
 * Author: Trevor Smith
 * Author URI: http://isrl.byu.edu.com/
 * License: None
 * Network: true
 * Text Domain: password-auth
 * Domain Path: /lang
 */

// Options page.
require_once 'PasswordAuthSettings.php';

if (!class_exists('PasswordAuth')) {
  require_once 'core/AuthCore.php';

  class PasswordAuth extends AuthCore {

    /**
     * Singleton code.
     */
    private static $instance = null;

    public static function get_instance() {
      if (null == self::$instance) {
        self::$instance = new self;
      }
      return self::$instance;
    }

    /**
     * Constructer.
     */
    function __construct() {
      parent::__construct();
    }

    /**
     * Gets a name that identifies this plugin. Should be unique from all other
     * authentication plugins. Used in various functions to identify this plugin.
     * It is also used as a query variable and should be so compatible.
     */
    protected function getName() {
      return "PasswordAuth";
    }

    /**
     * Gets the content to be put on the login page.
     */
    protected function getLoginFormContent() {
      if (isset($_POST['user_login'])) {
        $user_login = $_POST['user_login'];
      }
      $rememberme = !empty($_POST['rememberme']);
      ob_start();
      ?>

      <form name="loginform" id="PasswordAuthForm" action="<?php echo esc_url($this->getAuthenticateUrl()) ?>"
            method="post">
        <p>

        <h3>Log In</h3></p><br/>

        <p>
          <label for="user_login"><?php _e('Username'); ?><br>
            <input type="text" name="log" id="password_auth_user_login" class="input"
                   value="<?php echo esc_attr($user_login); ?>" size="20"></label>
        </p>

        <p>
          <label for="user_pass"><?php _e('Password'); ?><br>
            <input type="password" name="pwd" id="password_auth_user_pass" class="input"
                   value="" size="20"></label>
        </p>

        <p class="submit">
          <input type="submit" name="wp-submit" id="password_auth_wp-submit" class="button button-primary button-large"
                 value="<?php esc_attr_e('Log In'); ?>">
        </p>
      </form>

      <script>
        // Grab the redirect_to field.
        jQuery(document).ready(function () {
          jQuery('form#loginform input[name="redirect_to"]').clone().insertAfter(jQuery('#password_auth_wp-submit'));
        });
      </script>

      <?php
      return ob_get_clean();
    }

    /**
     * Authenticates the user.
     */
    protected function authenticateUser($user, $username = null, $password = null) {
      if (!empty($username)) {
        $user = get_user_by('login', $username);
        if (!$user) {
          return new WP_Error('invalid_username', __('<strong>ERROR</strong>: Invalid username.'));
        }
        $pass == $_POST['pwd'];
        if (!wp_check_password( $pass, $user->data->user_pass, $user->ID))
        {
          return new WP_Error('incorrect_password',
              __('<strong>ERROR</strong>: The password did not match the user account.'));
        }
        $this->setPasswordAuthEnabled($user->ID, true);
        return $user;
      }
    }

    /**
     * Gets the content to be put on the registration page.
     */
    protected function getRegisterFormContent() {
      $user_login = $_POST['user_login'];
      $useremail = $_POST['user_email'];

      ob_start();
      ?>

      <form name="PasswordAuthForm" id="PasswordAuthForm" action="<?php echo esc_url($this->getRegisterUrl()); ?>"
            onsubmit="return matchPasswords()" method="post">
        <p>

        <h3>Register</h3></p><br/>

        <p>
          <label for="user_login"><?php _e('Username') ?><br/></label>
          <input type="text" name="user_login" id="password_auth_user_login" class="input"
                 value="<?php echo esc_attr(wp_unslash($user_login)); ?>" size="20"/>
        </p>

        <p>
          <label for="user_email"><?php _e('Email') ?><br/></label>
          <input type="text" name="user_email" id="password_auth_user_email" class="input"
                 value=""<?php echo esc_attr(wp_unslash($useremail)); ?>"" size="20"/>
        </p>

        <p>
          <label for="password"><?php _e('Password'); ?><br>
            <input type="password" name="password" id="password_auth_user_pass" class="input"
                   value="" size="20"></label>
        </p>

        <p>
          <label for="user_verify"><?php _e('Verify Password'); ?><br>
            <input type="password" name="user_verify" id="password_auth_user_verify" class="input"
                   value="" size="20"></label>
        </p>

        <p class="submit">
          <input type="submit" name="password_auth_wp-submit" id="password_auth_wp-submit" class="button button-primary button-large"
                 value="<?php esc_attr_e('Register'); ?>">
        </p>
      </form>

      <script>
        function matchPasswords() {
          var pass1 = document.getElementById("password_auth_user_pass").value;
          var pass2 = document.getElementById("password_auth_user_verify").value;
          if (pass1 != pass2) {
            alert("The passwords do not match");
            document.getElementById("password_auth_user_pass").style.borderColor = "#E34234";
            document.getElementById("password_auth_user_verify").style.borderColor = "#E34234";
            return false;
          } else if (pass1 === "") {
            alert("A password is required");
            document.getElementById("password_auth_user_pass").style.borderColor = "#E34234";
            return false
          } else {
            return true;
          }
        }
      </script>

      <?php
      return ob_get_clean();
    }

    /**
     * Registers the user with this authentication method.
     */
    protected function registerUser($username, $usermail, $errors) {
      // If there is a problem we cannot register it.
      $newErrors = new WP_Error();
      foreach (array('empty_username', 'invalid_username', 'username_exists') as $code) {
        $this->copyErrors($errors, $newErrors, $code);
      }
      if ($newErrors->get_error_code() != '') {
        return $newErrors;
      }
     
      $userpass = $_POST['password'];
      $useremail = $_POST['user_email'];
      $userId = wp_create_user($username, $userpass, $useremail);
      if (!$userId || is_wp_error($userId)) {
        return $userId;
        $newErrors->add('registerfail',
            sprintf(__('<strong>ERROR</strong>: Couldn\'t register you&hellip; please contact the <a href="mailto:%s">webmaster</a> !'),
                get_option('admin_email')));
        return $newErrors;
      }

      $this->setPasswordAuthEnabled($userId, true);
      wp_new_user_notification($userId, $userpass);
      return $userId;
    }

    /**
     * Gets the content to be put on the user's profile page.
     */
    protected function getProfileContent($user) {
      ob_start();
      ?>

      <table class="form-table">
        <tbody>
        <tr>
          <th scope="row"><?php _e("Password Auth") ?></th>
          <td>
            <input name="enablePasswordAuth" id="enablePasswordAuth" type="checkbox"
                <?php checked($this->getPasswordAuthEnabled($user->ID)); ?> />
            <label for="enablePasswordAuth"><?php _e("Enable Password Auth for this account."); ?></label>
            <br/>
          </td>
        </tr>
        </tbody>
      </table>

      <?php
      return ob_get_clean();
    }

    /**
     * Updates the user's personal options.
     */
    protected function updatePersonalOptions($userId, $errors) {
      if (current_user_can('edit_user', $userId)) {
        $this->setPasswordAuthEnabled($userId, isset($_POST['enablePasswordAuth']));
      }
    }

    /**
     * Sets an option indicating the giver user can use Password Auth.
     */
    private function setPasswordAuthEnabled($userId, $enabled) {
      update_user_meta($userId, "{$this->getName()}_enabled", $enabled ? 'True' : 'False');
    }

    /**
     * Gets whether Password Auth can be used by the user.
     */
    private function getPasswordAuthEnabled($userId) {
      return get_user_meta($userId, "{$this->getName()}_enabled", true) == 'True' ? true : false;
    }
  }
}

/**
 * Starts PasswordAuth.
 */
function PasswordAuth() {
  return PasswordAuth::get_instance();
}

PasswordAuth();

?>
