<?php

/**
 * AuthCore is the core class for all the authentication plugins I am using. It was designed
 * to make each authentication similar in apperance and function. It also allows them to be
 * switched out on the fly for use in usability studies.
 *
 * NOTE: Public methods are those used by WordPress. This is neccessary so they can be called
 * with PHP's call_user_func_array();
 */
if (!class_exists('AuthCore')) {
  abstract class AuthCore {

    /**
     * Constructer. Registers the various WordPress hooks.
     */
    function __construct() {
      // Session management.
      add_action('parse_request', array($this, 'parseAuthenticationMethods'), 1);
      add_action('parse_request', array($this, 'parseAuthenticationMethodsFinalize'), 2);
      add_action('init', array($this, 'enqueueScripts'));
      add_filter('wp_session_expiration', function () { return 60 * 60; }); // One hour.
      add_filter('wp_session_expiration_variant', function () { return 5 * 60; }); // Five minutes.

      // Ajax methods.
      add_action('parse_request', array($this, 'ajaxIsLoggedIn'), 2);

      // Login page.
      add_action('login_head', array($this, 'removeShake'));
      add_action('login_form', array($this, 'displayLoginForm'));
      add_filter('authenticate', array($this, 'authenticateUserLogin'), 10, 3);
      add_filter('wp_login_errors', array($this, 'displayLoginErrors'));

      // Register page.
      add_action('register_form', array($this, 'displayRegisterForm'));
      add_action('register_post', array($this, 'registerUserPost'), 10, 3);
      add_filter('registration_errors', array($this, 'filterRegistrationErrors'), 10, 3);

      // Profile page.
      add_action('profile_personal_options', array($this, 'showUserProfile'));
      add_action('personal_options_update', array($this, 'personalOptionsUpdate'));
      add_filter('user_profile_update_errors', array($this, 'personalOptionsErrors'), 10, 3);
    }

    /**
     * Gets a name that identifies this plugin. Should be unique from all other
     * authentication plugins. Used in various functions to identify this plugin.
     * It is also used as a query variable and should be so compatible.
     */
    abstract protected function getName();


    // -----------------------------------------------------------------------------------------------
    // Session management
    // -----------------------------------------------------------------------------------------------

    /**
     * Sends a response that can be done cross domain.
     */
    protected function sendCorsResponse($content = null) {
      if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header("Access-Control-Expose-Headers: x-json");
      } else {
        header('Access-Control-Allow-Origin: *');
      }

      if ($content) {
        header('Content-Type: application/json');
        echo $content;
      }

      exit;
    }

    /**
     * Sets whether the plugin is enabled for the current user session based on the query string.
     */
    public function parseAuthenticationMethods() {
      if (isset($_GET['authentication'])) {
        // Disable the authentication method, and only re-enable it if we find
        // it in the array.
        $this->setEnabled(false);
        foreach (explode(',', $_GET['authentication']) as $method) {
          if ($method == $this->getName()) {
            $this->setEnabled(true);
            return;
          }
        }
      }
    }

    /**
     * If we are parsing authentication methods, stop doing so once we have our answer.
     */
    public function parseAuthenticationMethodsFinalize() {
      if (isset($_GET['authentication'])) {
        $this->sendCorsResponse();
      }
    }

    /**
     * Checks if the user is logged in.
     */
    public function ajaxIsLoggedIn() {
      if (isset($_GET['isLoggedIn'])) {
        if (is_user_logged_in()) {
          $content = '{"loggedIn": "true"}';
        } else {
          $content = '{"loggedIn": false}';
        }
        $this->sendCorsResponse($content);
      }
    }

    /**
     * Gets whether the plugin is enabled for the current user session.
     */
    protected function isEnabled() {
      $wpSession = WP_Session::get_instance();
      return isset($wpSession["{$this->getName()}_enabled"]);
    }

    /**
     * Sets whether the plubin is enabled for the current user session.
     */
    protected function setEnabled($enabled) {
      $wpSession = WP_Session::get_instance();
      if ($enabled) {
        $wpSession["{$this->getName()}_enabled"] = true;
      } else {
        unset($wpSession["{$this->getName()}_enabled"]);
      }
    }

    // -----------------------------------------------------------------------------------------------
    // General functions
    // -----------------------------------------------------------------------------------------------

    /**
     * Utitilty method that will copy all the errors with a give error_code from one WP_Error to
     * another.
     */
    protected function copyErrors($source, $dest, $code) {
      $items = $source->get_error_messages($code);
      foreach ($items as $item) {
        $dest->add($code, $item, $source->get_error_data($code));
      }
    }

    /**
     * Utitilty method that will copy all the errors from one WP_Error to another.
     */
    protected function addAllErrors($source, $dest) {
      foreach ($source->get_error_codes() as $code) {
        foreach ($source->get_error_messages($code) as $item) {
          $dest->add($code, $item, $source->get_error_data($code));
        }
      }
    }

    /**
     * Emits a script tag that contains code to show a modal dialog.
     */
    protected function showAuthCoreDialog($content, $imageUrl) {
      ?>
      <script>
        // Preload the image to prevent us seeing a broken img box.
        jQuery('<img src="<?php echo $imageUrl; ?>" />').load(function () {
          var dialogItem = jQuery(
              '<div>' +
              '  <table style="height: 123px">' +
              '  <tbody>' +
              '    <tr>' +
              '      <td style="padding: 10px;"><img src="<?php echo $imageUrl; ?>" style="height: 64px; width: 64px;" /></td>' +
              '      <td style="padding: 10px; vertical-align: middle; font-size: 28px; line-height: 32px; text-align: center"><?php echo $content; ?></td> ' +
              '    </tr>' +
              '  </tbody>' +
              '  </table>' +
              '</div>'
          );
          dialogItem.dialog({
            autoOpen: false,
            closeOnEscape: false,
            dialogClass: 'wp-dialog',
            draggable: false,
            modal: true,
            resizable: false,
            open: function (event, ui) {
              var parentWindow = jQuery(this).parent();
              jQuery(".ui-dialog-titlebar-close", parentWindow).hide();
              window.setTimeout(function () {
                parentWindow.position({my: 'center', at: 'center', of: window});
              }, 0);
            }
          });
          dialogItem.dialog("open");
        });
      </script>
    <?php
    }


    /**
     * Remove the shake effect seen at the login page when errors are created. Does not do well
     * when we add additional forms.
     */
    public function removeShake() {
      remove_action('login_head', 'wp_shake_js', 12);
    }

    /**
     * Enqueues the jquery script on the login page.
     */
    public function enqueueScripts() {
      wp_enqueue_script('jquery');
      wp_enqueue_script('jquery-ui-dialog');
      wp_enqueue_style('wp-jquery-ui-dialog');
    }

    /**
     * Inserts the new form in to the login and register pages.
     */
    public function insertNewLoginRegisterForm($content, $defaultFormName, $removeDefaultForm) {
      ?>
      <script>
        // Remove the default authentication if desired.
        var defaultForm = jQuery('<?php echo $defaultFormName ?>');
        var defaultShown = defaultForm.is(':visible');
        var removeDefault = <?php echo $removeDefaultForm ? 'true' : 'false'; ?>;
        if (removeDefault) {
          defaultForm.hide();
        }

        // Add the new authentication method.
        newItem = jQuery(
            "<div id='<?php echo $this->getName(); ?>'>" +
            "<br />" +
            <?php echo json_encode($content); ?> +
                "</div>");
        newItem.insertBefore(defaultForm);

        // Add text saying "or" if needed. Special rules needed since default auth is always at the
        // bottom and new auth methods are added before it, resulting in them being the order added.
        var orItem = jQuery('<br /><h3 style="text-align:center;">&mdash; <?php _e('OR'); ?> &mdash;</h3>');
        if (defaultShown && !removeDefault) { // Default authentication still shown.
          orItem.insertAfter(newItem);
        } else if (!defaultShown) { // Default was not shown even before this method.
          orItem.insertBefore(newItem);
        }
      </script>
    <?php
    }


    // -----------------------------------------------------------------------------------------------
    // Login form
    // -----------------------------------------------------------------------------------------------

    /**
     * Gets the content to be put on the login page.
     */
    abstract protected function getLoginFormContent();

    /**
     * Authenticates the user.
     */
    abstract protected function authenticateUser($user, $username = null, $password = null);

    /**
     * Gets whether to remove the default login form.
     */
    protected function removeDefaultLogin() {
      return get_option("{$this->getName()}_authEnabled", 'True') === 'True' ? false : true;
    }

    /**
     * Get the url for posting authentication requests.
     */
    protected function getAuthenticateUrl() {
      return add_query_arg($this->getName(), 'login', site_url('wp-login.php', 'login_post'));
    }

    /**
     * Get the url for posting authentication requests.
     */
    protected function getRegisteredUrl() {
      return add_query_arg('registered', $this->getName(), site_url('wp-login.php', 'login_post'));
    }

    /**
     * If this plugin is enabled for the current user session adds a new login form.
     */
    public function displayLoginForm() {
      if ($this->isEnabled()) {
        $this->insertNewLoginRegisterForm($this->getLoginFormContent(), 'form#loginform',
            $this->removeDefaultLogin());
      }
    }

    /**
     * Authenticates the user.
     */
    public function authenticateUserLogin($user, $username = null, $password = null) {
      if ($this->isEnabled() && isset($_REQUEST[$this->getName()])) {
        $user = $this->authenticateUser($user, $username, $password);
        if ($user && !is_wp_error($user) && $user->user_login == 'admin') {
          return new WP_Error('auth', 'Cannot use this authentication method to login as admin.');
        }
        return $user;
      } else {
        return $user;
      }
    }

    /**
     * Display any custom login errors we want.
     *
     * For now we just add a message, which is implemented as a WP_Error, that tells users when
     * they have successfully registered.
     */
    public function displayLoginErrors($errors) {
      if ($this->isEnabled() && isset($_REQUEST['registered']) &&
          $_REQUEST['registered'] == $this->getName()
      ) {
        $errors->add('registered', __('Registration complete. Please log in to continue.'), 'message');
      }
      return $errors;
    }

    // ---------------------------------------------------------------------------------------------
    // Register form
    // ---------------------------------------------------------------------------------------------

    /**
     * Gets the content to be put on the registration page.
     */
    abstract protected function getRegisterFormContent();

    /**
     * Register a new user.
     */
    abstract protected function registerUser($username, $usermail, $errors);


    /**
     * Gets whether to remove the default registration form.
     */
    protected function removeDefaultRegistration() {
      return get_option("{$this->getName()}_registerEnabled", 'True') === 'True' ? false : true;
    }

    /**
     * Get the url for posting authentication requests.
     */
    protected function getRegisterUrl() {
      return add_query_arg($this->getName(), 'register', site_url('wp-login.php?action=register', 'login_post'));
    }

    /**
     * Displays the register form for this authentication if it is enabled.
     */
    public function displayRegisterForm() {
      if ($this->isEnabled()) {
        $this->insertNewLoginRegisterForm($this->getRegisterFormContent(), 'form#registerform',
            $this->removeDefaultRegistration());
      }
    }

    /**
     * Called when a user is being created. This method will hijack user creation if successful,
     * otherwise it will store errors into the session that will be displayed by
     * filterRegistrationErrors.
     */
    public function registerUserPost($username, $usermail, $errors) {
      if ($this->isEnabled() && isset($_REQUEST[$this->getName()])) {
        $wpSession = WP_Session::get_instance();
        unset($wpSession["{$this->getName()}_registrationError"]);
        $userId = $this->registerUser($username, $usermail, $errors);

        if (is_wp_error($userId)) {
          $wpSession["{$this->getName()}_registrationError"] = $userId;
        } else {
          wp_set_auth_cookie($userId, false, is_ssl());
          $redirect_to = !empty($_POST['redirect_to']) ? $_POST['redirect_to'] : site_url('/', 'https');
          wp_safe_redirect($redirect_to);
          exit();
        }
      }
    }

    /**
     * Dispalys any errors encountered during registerUserPost. This replaces other errors that
     * might have been found.
     */
    public function filterRegistrationErrors($errors, $sanitized_user_login, $user_email) {
      if ($this->isEnabled() && isset($_REQUEST[$this->getName()])) {
        $wpSession = WP_Session::get_instance();
        if (isset($wpSession["{$this->getName()}_registrationError"])) {
          $errors = $wpSession["{$this->getName()}_registrationError"];
        }
      }
      return $errors;
    }

    // -----------------------------------------------------------------------------------------------
    // Profile page
    // -----------------------------------------------------------------------------------------------

    /**
     * Gets the content to be put on the user's profile page.

     */
    abstract protected function getProfileContent($user);

    /**
     * Updates the user's personal options.
     */
    abstract protected function updatePersonalOptions($userId, $errors);

    /**
     * Modify the user profile page.
     */
    public function showUserProfile($user) {
      if ($this->isEnabled()) {
        echo $this->getProfileContent($user);
      }
    }

    /**
     * Updates the current user's personal options.
     */
    public function personalOptionsUpdate($userId) {
      $errors = new WP_Error();
      $this->updatePersonalOptions($userId, $errors);
      $wpSession = WP_Session::get_instance();
      $wpSession["{$this->getName()}_profileUpdateError"] = $errors;
    }

    /**
     * Displays errors encountered while updating the user.
     */
    public function personalOptionsErrors($errors, $update, $user) {
      $wpSession = WP_Session::get_instance();
      if (isset($wpSession["{$this->getName()}_profileUpdateError"])) {
        $this->addAllErrors($wpSession["{$this->getName()}_profileUpdateError"], $errors);
      }
    }

  }
}

?>
