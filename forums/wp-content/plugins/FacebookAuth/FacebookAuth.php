<?php

/**
 * Plugin Name: Facebook Auth
 * Plugin URI: http://isrlresearch.byu.edu/
 * Description: A plugin that allows loging in with Facebook.
 * Version: 0.1
 * Author: Scott Ruoti
 * Author URI: http://isrl.byu.edu.com/
 * License: None
 * Network: true
 * Text Domain: facebook-auth
 * Domain Path: /lang
 */

// Options page.
require_once 'FacebookAuthSettings.php';

if (!class_exists('FacebookAuth')) {
  require_once 'core/AuthCore.php';

  class FacebookAuth extends AuthCore {

    /**
     * Singleton code.
     */
    private static $instance = null;

    public static function getInstance() {
      if (null == self::$instance) {
        self::$instance = new self;
      }
      return self::$instance;
    }

    /**
     * Reference to the facebook class.
     */
    private $facebookClient = null;

    /**
     * Constructer.
     */
    function __construct() {
      parent::__construct();
    }

    /**
     * Gets the facebook client for this instance.
     */
    protected function getFacebookClient() {
      if (!class_exists('Facebook')) {
        require_once 'facebook-php-sdk/facebook.php';
      }

      if (null == $this->facebookClient) {
        $this->facebookClient = new Facebook(array(
            'appId'  => get_option("{$this->getName()}_appId", ''),
            'secret' => get_option("{$this->getName()}_secret", '')
        ));
      }
      return $this->facebookClient;
    }

    /**
     * Gets a verified user. Used to process an authentication response.
     */
    protected function getVerifiedUser() {
      // Destroying the session forces us to have come from a valid FB login.
      $facebook = $this->getFacebookClient();
      $facebook->destroySession();
      $fbUser = $facebook->getUser();

      if ($fbUser) {
        try {
          // Force a call to the Facebook API to ensure the app still has permissions..
          $user_profile = $facebook->api('/me');
          return array($fbUser, $user_profile['email']);
        } catch (FacebookApiException $e) {
          return new WP_Error('facebook', __('<strong>ERROR</strong>: Unable to verify account with Facebook.'));
        }
      } else {
        return new WP_Error('facebook', __('<strong>ERROR</strong>: Unable to verify account with Facebook.'));
      }
    }

    /**
     * Gets the auth URI to use.
     */
    protected function redirectToAuthUrl($redirectUri) {
      $facebook = $this->getFacebookClient();
      wp_redirect($facebook->getLoginUrl(array(
          'redirect_uri' => $redirectUri,
          'scope'        => 'email'
      )));
      exit;
    }

    /**
     * Gets a name that identifies this plugin. Should be unique from all other
     * authentication plugins. Used in various functions to identify this plugin.
     * It is also used as a query variable and should be so compatible.
     */
    protected function getName() {
      return "FacebookAuth";
    }

    /**
     * Gets the content to be put on the login page.
     */
    protected function getLoginFormContent() {
      ob_start();
      ?>

      <div id="FacebookAuthDiv">
        <a id="FacebookAuth_url" href="#" rel="nofollow">
          <img style="display: block; margin: auto"
               src="<?php echo esc_url(plugins_url('img/FacebookLogin.png', __FILE__)) ?>"/>
        </a>
      </div>
      <form name="FacebookAuthForm" id="FacebookAuthForm" style="display: none;" method="post"
            action="<?php echo esc_url($this->getAuthenticateUrl()); ?>">
      </form>

      <script>
        // Clicking the link submits the form.
        jQuery('div#FacebookAuthDiv a#FacebookAuth_url').on('click', function () {
          jQuery('form#FacebookAuthForm').trigger('submit');
          return false;
        });

        // Grab redirect_to from the main form.
        jQuery(document).ready(function () {
          jQuery('form#loginform input[name="redirect_to"]').clone().appendTo(jQuery('form#FacebookAuthForm'));
        });
      </script>

      <?php
      return ob_get_clean();
    }

    /**
     * Authenticates the user.
     */
    protected function authenticateUser($user, $username = null, $password = null) {
      // Respond differently to the login request and the facebook redirect.
      if ($_REQUEST[$this->getName()] == 'fblogin') {
        $fbInfo = $this->getVerifiedUser();
        if (is_wp_error($fbInfo)) {
          return $fbInfo;
        }
        $fbUser = $fbInfo[0];

        $user = $this->getUserByFacebookId($fbUser);
        if (!$user) {
          return new WP_Error('facebook',
              __('<strong>ERROR</strong>: No user is registered with the your current Facebook account.'));
        }
        return $user;
      } else {
        // Authenticate user using facebook.
        $redirectUri = add_query_arg("{$this->getName()}", 'fblogin', $this->getAuthenticateUrl());
        if (isset($_REQUEST['redirect_to']) && $_REQUEST['redirect_to'] != '') {
          $redirectUri = add_query_arg("redirect_to", urlencode($_REQUEST['redirect_to']), $redirectUri);
        }
        $this->redirectToAuthUrl($redirectUri);
      }
    }

    /**
     * Gets the content to be put on the registration page.
     */
    protected function getRegisterFormContent() {
      // Otherwise return the login form.
      if (isset($_POST['user_login'])) {
        $user_login = $_POST['user_login'];
      }

      ob_start();
      ?>

      <form name="FacebookAuthForm" id="FacebookAuthForm" action="<?php echo esc_url($this->getRegisterUrl()); ?>"
            method="post">
        <p>
          <label for="user_login"><?php _e('Username') ?><br/></label>
          <input type="text" name="user_login" id="facebook_auth_user_login" class="input"
                 value="<?php echo esc_attr(wp_unslash($user_login)); ?>" size="20"/>
        </p>
        <a id="FacebookAuth_url" href="#" rel="nofollow">
          <img style="display: block; margin: auto"
               src="<?php echo esc_url(plugins_url('img/FacebookRegister.png', __FILE__)) ?>"/>
        </a>
      </form>

      <script>
        // Clicking the link submits the form.
        jQuery('form#FacebookAuthForm a#FacebookAuth_url').on('click', function () {
          jQuery('form#FacebookAuthForm').trigger('submit');
          return false;
        });

        // Grab redirect_to from the main form.
        jQuery(document).ready(function () {
          jQuery('form#registerform input[name="redirect_to"]').clone().appendTo(jQuery('form#FacebookAuthForm'));
        });
      </script>

      <?php
      return ob_get_clean() . $this->transformRegisterGetToPost();
    }

    /**
     * Gets HTML that will transform the current get request to a post request if neccessary for
     * registering the user. This is done since register_new_user() is only called for POST
     * requests.
     */
    protected function transformRegisterGetToPost() {
      if ($_SERVER['REQUEST_METHOD'] === 'GET' &&
          isset($_REQUEST[$this->getName()]) && $_REQUEST[$this->getName()] == 'fbregister'
      ) {
        $wpSession = WP_Session::get_instance();
        $username = isset($wpSession["{$this->getName()}_username"]) ? $wpSession["{$this->getName()}_username"] : '';

        ob_start();
        ?>

        <form name="FacebookSubmitForm" id="FacebookSubmitForm" method="post" style="display: none;"
              action="<?php echo esc_url($_SERVER['REQUEST_URI']); ?>">
          <input type="text" name="user_login" value="<?php echo esc_attr($username); ?>"/>
        </form>

        <?php $this->showAuthCoreDialog('Creating Account', plugins_url('img/loader.gif', __FILE__)); ?>

        <script>
          // Immediately submit the form.
          jQuery(document).ready(function () {
            jQuery('form#FacebookSubmitForm').trigger('submit');
          });
        </script>

        <?php
        return ob_get_clean();
      }
      return '';
    }

    /**
     * Authenticates the user with this authentication method.
     */
    protected function registerUser($username, $usermail, $errors) {
      // If there is a problem with the username we cannot register it.
      $newErrors = new WP_Error();
      foreach (array('empty_username', 'invalid_username', 'username_exists') as $code) {
        $this->copyErrors($errors, $newErrors, $code);
      }
      if ($newErrors->get_error_code() != '') {
        return $newErrors;
      }

      // If this is the response to the facebook register request, register the user.
      if ($_REQUEST[$this->getName()] == 'fbregister') {
        $fbInfo = $this->getVerifiedUser();
        if (is_wp_error($fbInfo)) {
          return $fbInfo;
        }
        $fbUser = $fbInfo[0];

        // Ensure there is not a user with this account.
        $user = $this->getUserByFacebookId($fbUser);
        if ($user) {
          return new WP_Error('facebook',
              __('<strong>ERROR</strong>: A user is already associated with this Facebook account.'));
        }

        $usermail = $fbInfo[1];
        $userpass = wp_generate_password(12, false);
        $userId = wp_create_user($username, $userpass, $usermail);
        if (is_wp_error($userId)) {
          if ($userId->get_error_code() == 'existing_user_email') {
            return new WP_Error('facebook', '<strong>ERROR</strong>: The email associated with the ' .
                'current Facebook account is already in use on this site.');
          } else {
            return $userId;
          }
        }
        if (!$userId) {
          return new WP_Error('registerfail',
              sprintf(__('<strong>ERROR</strong>: Couldn\'t register you&hellip; please contact the ' .
                  '<a href="mailto:%s">webmaster</a> !'), get_option('admin_email')));
        }
        $this->setFacebookId($userId, $fbUser);
        return $userId;
      } else {
        // Store the username. We do this since it cannot have come through post on the redirect.
        $wpSession = WP_Session::get_instance();
        $wpSession["{$this->getName()}_username"] = $username;

        // Register user using facebook.
        $redirectUri = add_query_arg("{$this->getName()}", 'fbregister', $this->getRegisterUrl());
        if (isset($_REQUEST['redirect_to']) && $_REQUEST['redirect_to'] != '') {
          $redirectUri = add_query_arg("redirect_to", urlencode($_REQUEST['redirect_to']), $redirectUri);
        }
        $this->redirectToAuthUrl($redirectUri);
      }
    }

    /**
     * Gets the content to be put on the user's profile page.
     */
    protected function getProfileContent($user) {
      // If the user can't login normally, remove the options to change the linking of the account.
      if ($this->removeDefaultLogin()) {
        return '';
      }

      $fbUser = $this->getFacebookId($user->ID);
      $innerText = '';

      // Unlink the account.
      if ($fbUser) {
        ob_start();
        ?>
        <a id="FacebookAuth_url" href="#" rel="nofollow">
          <img style="display: block;"
               src="<?php echo esc_url(plugins_url('img/FacebookUnlink.png', __FILE__)) ?>"/>
        </a>
        <br/>
        <span class="description">
        <?php _e('Unlinking your account will remove the ability to use Facebook to log in to this site.'); ?>
      </span>

        <script>
          var button = jQuery('form a#FacebookAuth_url');
          button.on('click', function () {
            jQuery('form input[type="hidden"]#facebookAction').val('unlink');
            jQuery('form input[type="submit"]#submit').trigger('click');
            return false;
          });
        </script>
      <?php
      } else {
        // Link the account.
        ob_start();
        ?>
        <a id="FacebookAuth_url" href="#" rel="nofollow">
          <img style="display: block;"
               src="<?php echo esc_url(plugins_url('img/FacebookLink.png', __FILE__)) ?>"/>
        </a>
        <br/>
        <span class="description">
        <?php _e('Linking your account will allow you to use Facebook to log in to this site.'); ?>
      </span>

        <script>
          var button = jQuery('form a#FacebookAuth_url');
          button.on('click', function () {
            jQuery('form input[type="hidden"]#facebookAction').val('link');
            jQuery('form input[type="submit"]#submit').trigger('click');
            return false;
          });
        </script>
      <?php
      }
      $innerText = ob_get_clean();

      ob_start();
      ?>

      <h3>Facebook Authentication</h3>
      <table class="form-table">
        <tbody>
        <tr>
          <th scope="row"><?php _e("Facebook Account") ?></th>
          <td>
            <input type='hidden' id='facebookAction' name='facebookAction' value=''/>
            <?php echo $innerText; ?>
            <br/>
          </td>
        </tr>
        </tbody>
      </table>

      <?php
      return ob_get_clean() . $this->transformLinkGetToPost();
    }

    /**
     * Gets HTML that will transform the current get request to a post request if neccessary for
     * registering the user. This is done since register_new_user() is only called for POST
     * requests.
     */
    protected function transformLinkGetToPost() {
      if ($_SERVER['REQUEST_METHOD'] === 'GET' &&
          isset($_REQUEST[$this->getName()]) && $_REQUEST[$this->getName()] == 'link'
      ) {
        ob_start();
        ?>

        <?php $this->showAuthCoreDialog('Updating account', plugins_url('img/loader.gif', __FILE__)); ?>

        <script>
          // Immediately submit the form.
          jQuery(document).ready(function () {
            var form = jQuery('form');
            console.log(form.attr('action'));
            form.attr('action', '<?php echo $_SERVER['REQUEST_URI']; ?>');
            jQuery('input[type="submit"]#submit', form).trigger('click');
          });
        </script>

        <?php
        return ob_get_clean();
      }
      return '';
    }

    /**
     * Updates the user's personal options.
     */
    protected function updatePersonalOptions($userId, $errors) {
      // If the user can't login normally, remove the options to change the linking of the account.
      if ($this->removeDefaultLogin()) {
        return;
      }

      if (current_user_can('edit_user', $userId) && isset($_POST['facebookAction'])) {
        $facebookAction = $_POST['facebookAction'];
        if ($facebookAction == 'unlink') {
          $this->setFacebookId($userId, '');
        } elseif ($facebookAction == 'link') {
          // Link the user account to facebook.
          $redirectUri = add_query_arg("{$this->getName()}", 'link', self_admin_url('profile.php'));
          $this->redirectToAuthUrl($redirectUri);
        } elseif ($_REQUEST[$this->getName()] == 'link') {
          $fbInfo = $this->getVerifiedUser();
          if (is_wp_error($fbInfo)) {
            $this->copyErrors($fbInfo, $errors, 'facebook');
            return;
          }
          $fbUser = $fbInfo[0];

          // Ensure only one user has this facebook ID.
          $user = $this->getUserByFacebookId($fbUser);
          if ($user) {
            $errors->add('facebook',
                __('<strong>ERROR</strong>: A user is already associated with this Facebook account.'));
            return;
          }
          $this->setFacebookId($userId, $fbUser);
        }
      }
    }

    /**
     * Sets an option indicating the given user's facebook id.
     */
    private function setFacebookId($userId, $facebookId) {
      update_user_meta($userId, "{$this->getName()}_id", $facebookId);
    }

    /**
     * Gets an option setting the given user's facebook id.
     */
    private function getFacebookId($userId) {
      return intval(get_user_meta($userId, "{$this->getName()}_id", true));
    }

    /**
     * Gets the user with the given facebook Id.
     */
    private function getUserByFacebookId($facebookId) {
      $users = get_users(array('meta_key' => "{$this->getName()}_id", 'meta_value' => $facebookId));
      if (empty($users)) {
        return null;
      } else {
        return $users[0];
      }
    }

  }
}

/**
 * Starts BasicAuth.
 */
function FacebookAuth() {
  return FacebookAuth::getInstance();
}

FacebookAuth();

?>
