<?php

/**
 * Plugin Name: WebTicket Auth
 * Plugin URI: http://isrlresearch.byu.edu/
 * Description: A plugin that allows logging in with WebTicket.
 * Version: 0.1
 * Author: Scott Ruoti
 * Author URI: http://isrl.byu.edu.com/
 * License: None
 * Network: true
 * Text Domain: webticket-auth
 * Domain Path: /lang
 */

// Options page.
require_once 'WebTicketAuthSettings.php';

if (!class_exists('WebTicketAuth')) {
  require_once 'core/AuthCore.php';

  class WebTicketAuth extends AuthCore {

    /**
     * Singleton code.
     */
    private static $instance = null;

    public static function getInstance() {
      if (null == self::$instance) {
        self::$instance = new self;
      }
      return self::$instance;
    }

    /**
     * Constructer.
     */
    function __construct() {
      parent::__construct();
      add_action('init', array($this, 'enqueueQRReaderScripts'));
    }

    /**
     * Gets a name that identifies this plugin. Should be unique from all other
     * authentication plugins. Used in various functions to identify this plugin.
     * It is also used as a query variable and should be so compatible.
     */
    protected function getName() {
      return "WebTicketAuth";
    }

    /**
     * Enqueues the jquery script on the login page.
     */
    public function enqueueQRReaderScripts() {
      wp_enqueue_script('jquery-html5-qrcode', plugins_url('js/html5-qrcode.min.js', __FILE__), array('jquery'));
    }

    /**
     * Show the WebTicket image.
     */
    protected function getLoginFormContent() {
      ob_start();
      ?>
      <form name="WebTicketLogin" id="WebTicketLogin" action="<?php echo esc_url($this->getAuthenticateUrl()); ?>"
            method="POST"
            style="width: 394px; margin-left: -61px;">

        <h3 align="center" style="margin-bottom: 15px;">
          Display your WebTicket to log in.
        </h3>


        <div id="WebTicketReader" style="width: 384px; height: 288px;  border: solid 5px; transform: rotateY(180deg);"/>

        <input type="text" name="WebTicket_URL" id="WebTicket_URL" style="display: none;">
        <input type="text" name="WebTicket_Username" id="WebTicket_Username" style="display: none;">
        <input type="password" name="WebTicket_Password" id="WebTicket_Password" style="display: none"/>
      </form>


      <script>
        jQuery(function () {
          var alreadySubmitted = false;

          jQuery('#WebTicketReader').html5_qrcode(function (data) {
                console.log('Success: ' + data);
                if (alreadySubmitted) {
                  return;
                }

                var lines = data.split("\n");
                if (lines.length != 3) {
                  <?php $this->showMessageDialog('Error reading the presented WebTicket. Please try again.'); ?>
                  return;
                }

                // Set the paramaters based on the WebTicket and submit the form.
                alreadySubmitted = true;
                jQuery("#WebTicket_URL").val(lines[2]);
                jQuery("#WebTicket_Username").val(lines[0]);
                jQuery("#WebTicket_Password").val(lines[1]);
                jQuery("#WebTicketLogin").submit();
              },
              function (error) {
                console.log(error);
              },
              function (videoError) {
                <?php $this->showMessageDialog('Unable to access web cam. Please reload the page and try again.'); ?>
              }
          );
        });
      </script>
      <?php
      return ob_get_clean();
    }

    /**
     * Log in the user if the crednetials match.
     */
    protected function authenticateUser($user, $username = null, $password = null) {
      // Avoid infinite recursion.
      if ($_POST['WebTicket_Handled']) {
        return;
      }

      if (!isset($_POST['WebTicket_URL']) || $_POST['WebTicket_URL'] != $this->getAuthenticateUrl()) {
        return new WP_Error('websticket',
            __('<strong>ERROR</strong>: You seem to be using a WebTicket for another website. Ensure that you are using the correct WebTicket and try again.'));
      }

      $_POST['WebTicket_Handled'] = true;
      $user = wp_signon(array('user_login'    => $_POST['WebTicket_Username'],
                             'user_password' => $_POST['WebTicket_Password']), true);
      if(is_wp_error($user))
        new WP_Error('websticket',
            __('<strong>ERROR</strong>: Incorrect credentials. Ensure that you are using the correct WebTicket and try again.'));
      else
        return $user;
    }

    /**
     * Show the register form.
     */
    protected
    function getRegisterFormContent() {
      $user_login = $_POST['user_login'];
      $user_email = $_POST['user_email'];

      ob_start();
      ?>
      <form name="WebTicketRegister" id="WebTicketRegister" action="<?php echo esc_url($this->getRegisterUrl()); ?>"
            method="post">
        <p>
          <label for="user_login"><?php _e('Username') ?><br/>
            <input type="text" name="user_login" id="user_login" class="input"
                   value="<?php echo esc_attr(wp_unslash($user_login)); ?>" size="20"/></label>
        </p>

        <p>
          <label for="user_email"><?php _e('E-mail') ?><br/>
            <input type="text" name="user_email" id="user_email" class="input"
                   value="<?php echo esc_attr(wp_unslash($user_email)); ?>" size="25"/></label>
        </p>

        <p id="reg_passmail"><?php _e('A WebTicket will be mailed to you.') ?></p>
        <br class="clear"/>

        <p class="submit"><input type="submit" name="wp-submit" id="wp-submit"
                                 class="button button-primary button-large"
                                 value="<?php esc_attr_e('Register'); ?>"/></p>
      </form>
      <?php
      return ob_get_clean();
    }

    /**
     * Authenticates the user with this authentication method.
     */
    protected
    function registerUser($username, $usermail, $errors) {
      $errors = apply_filters('registration_errors', $errors, $username, $usermail);

      if ($errors->get_error_code()) {
        return $errors;
      }

      $user_pass = wp_generate_password(12, false);
      $user_id = wp_create_user($username, $user_pass, $usermail);
      if (!$user_id || is_wp_error($user_id)) {
        $errors->add('registerfail',
            sprintf(__('<strong>ERROR</strong>: Couldn&#8217;t register you&hellip; please contact the <a href="mailto:%s">webmaster</a> !'),
                get_option('admin_email')));
        return $errors;
      }
      add_user_meta($user_id, 'user_password', $user_pass, true);

      // URL to QR code.
      $url = $this->getWebTicketUrl($username, $user_pass);
      $file = "<a href='{$url}'>WebTicket</a>";

      // Send e-mail with QR code for WebTicket.
      $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
      $message = __('Please print out the linked WebTicket to log into the site. You will also find your code to ' .
              'complete your task at the bottom of the WebTicket.') . "<br /><br />";
      $message .= $file;
      add_filter('wp_mail_content_type', function () { return "text/html"; });
      wp_mail($usermail, sprintf(__('[%s] Verify Registration (%s)'), $blogname, date('Y/m/d g:i:sA')), $message);

      return $user_id;
    }

    /**
     * Called when a user is being created. This method will hijack user creation if successful,
     * otherwise it will store errors into the session that will be displayed by
     * filterRegistrationErrors.
     */
    public
    function registerUserPost($username, $usermail, $errors) {
      if ($this->isEnabled() && isset($_REQUEST[$this->getName()])) {
        $wpSession = WP_Session::get_instance();
        unset($wpSession["{$this->getName()}_registrationError"]);
        $userId = $this->registerUser($username, $usermail, $errors);

        if (is_wp_error($userId)) {
          $wpSession["{$this->getName()}_registrationError"] = $userId;
        } else {
          wp_safe_redirect(add_query_arg('registered', $this->getName(), site_url('wp-login.php')));
          exit();
        }
      }
    }

    /**
     * Display any custom login errors we want.
     *
     * For now we just add a message, which is implemented as a WP_Error, that tells users when
     * they have successfully registered.
     */
    public
    function displayLoginErrors($errors) {
      if ($this->isEnabled() && isset($_REQUEST['registered']) &&
          $_REQUEST['registered'] == $this->getName()
      ) {
        $errors->add('registered', __('Please check your e-mail to retrieve your WebTicket. '), 'message');
      }
      return $errors;
    }

    /**
     * Adds a short method describing how to use SAW.
     */
    protected
    function getProfileContent($user) {
      $username = $user->user_login;
      $user_pass = get_user_meta($user->ID, "user_password", true);

      $url = $this->getWebTicketUrl($username, $user_pass);
      $file = "<a href='{$url}'>WebTicket</a>";

      ob_start();
      ?>

      <label for="email"><h2 style="color: red;">WebTicket Authentication</h2></label>
      <table class="form-table">
        <tbody>
        <tr>
          <th scope="row"><?php _e("WebTicket") ?></th>
          <td>
            To use WebTicket with this account, please print out the WebTicket linked below.
            You will find your code to complete the task at the bottom of the WebTicket. <br/><br/>
            <?php echo $file; ?>
            <br/>
          </td>
        </tr>
        </tbody>
      </table>

      <?php
      return ob_get_clean();
    }

    /**
     * Not used in Saw.
     */
    protected function updatePersonalOptions($userId, $errors) {
    }

    /**
     * Get the URL for the web ticket.
     */
    protected function getWebTicketUrl($username, $password) {
      return add_query_arg(array('baseUrl'  => base64_encode(plugins_url('lib', __FILE__) . '/'),
                                 'qrData'   => base64_encode("{$username}\n{$password}\n{$this->getAuthenticateUrl()}"),
                                 'blogName' => base64_encode(wp_specialchars_decode(get_option('blogname'),
                                         ENT_QUOTES)),
                                 'userName' => base64_encode($username)),
          plugins_url('lib/WebTicket.php', __FILE__));
    }

    /**
     * Emits a script tag that contains code to show a modal dialog.
     */
    protected function showMessageDialog($content) {
      ?>
      var dialog = jQuery("<div></div>").dialog({
        buttons: [{
          text: 'Ok',
          click: function () { jQuery(this).dialog("close"); },
          'class': 'small-button'
        }],
        close: function (event, ui) { jQuery(this).remove(); },
        resizable: false,
        modal: true,
        create: function() {
          jQuery(".ui-dialog-titlebar").hide();
          jQuery(this).parent().addClass("ui-state-error");
        }
      }).text("<?php echo $content; ?>");
    <?php
    }
  }
}

/**
 * Starts WebTicketAuth.
 */
function WebTicketAuth() {
  return WebTicketAuth::getInstance();
}

WebTicketAuth();

?>
