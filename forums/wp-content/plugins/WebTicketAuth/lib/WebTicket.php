<?php

require_once('dompdf/dompdf_config.inc.php');
date_default_timezone_set('America/Denver');

// Generate the URL.
$baseURL = base64_decode($_GET['baseUrl']);

$qrUrl = $baseURL . 'QR.php?data=' . $_GET['qrData'];
$qrImg = "<img src='{$qrUrl}' style='display: block; margin: 0px 0px 0px 9px;' />";

$scissorsUrl = $baseURL . 'scissors.png';
$scissorsImg = "<img src='{$scissorsUrl}' style='position: absolute; left: 280px; top: 610px;' />";

$blogName = base64_decode($_GET['blogName']);
$userName = base64_decode($_GET['userName']);

$code = rand();
if (strpos($blogName, 'Smartphone') === 0) {
  while ($code >= 1000000000) {
    $code = rand();
  }
} else {
  if (strpos($blogName, 'Bank') === 0) {
    while ($code < 1000000000) {
      $code = rand();
    }
  }
}

//$code = hexdec(substr(hash('md5', $qrUrl), 0, 8));

ob_start();

?>
  <page>
    <head>
      <style>
        @page {
          margin: 1in;
        }
      </style>
    </head>
    <body>
    <div style="width: 270px; height: 625px; padding: 1px; border: dashed 2px;">
      <div style="width: 250px; height: 605px; border: solid lightblue 10px;">
        <div style="width: 250px; height: 300px; border-bottom: solid 5px;">
          <h3 align="center" style="margin: 30px 0px 40px 0px; padding: 5px;">

            WebTicket for <br/>
            <?php echo $blogName; ?> <br/>
            Username: <?php echo $userName ?><br/><br/>

            To log in, show this ticket at the <?php echo $blogName; ?> login page.
          </h3>

          <div style="width: 250px; border-top: double 3px;">
            <h3 align="center" style="margin: 15px 0px 0px 0px; font-style: italic">Hold Here</h3>
          </div>
        </div>
        <div
            style="width: 250px; height: 300px; transform: rotate(180); position: absolute; left: 187; top: 463;">
          <?php echo $qrImg; ?>
        </div>
      </div>
    </div>

    <?php echo $scissorsImg; ?>

    <br/><br/><br/><br/><br/><br/>
    <h4>Your code to complete the task is: <?php echo $code; ?></h4>

    <div>
      <h4 style="position: absolute; left: 285px; top: 630px;">1) Cut here</h4>
    </div>

    <div>
      <h4 style="position: absolute; left: 285px; top: 282px;">2) Fold here</h4>
    </div>

    </body>
  </page>
<?php

$content = ob_get_clean();

$dompdf = new DOMPDF();
$dompdf->set_paper('letter');
$dompdf->load_html($content);
$dompdf->render();
$dompdf->stream("WebTicket.pdf", array('Attachment' => 0));

?>