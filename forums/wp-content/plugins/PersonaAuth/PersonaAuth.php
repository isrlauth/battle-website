<?php

/**
 * Plugin Name: Persona Auth
 * Plugin URI: http://isrlresearch.byu.edu/
 * Description: A plugin that allows loging in with Persona.
 * Version: 0.1
 * Author: Scott Ruoti
 * Author URI: http://isrl.byu.edu.com/
 * License: None
 * Network: true
 * Text Domain: persona-auth
 * Domain Path: /lang
 */

// Options page.
require_once 'PersonaAuthSettings.php';

if (!class_exists('PersonaAuth')) {
  require_once 'core/AuthCore.php';

  class PersonaAuth extends AuthCore {

    /**
     * Singleton code.
     */
    private static $instance = null;

    public static function getInstance() {
      if (null == self::$instance) {
        self::$instance = new self;
      }
      return self::$instance;
    }

    /**
     * Constructer.
     */
    function __construct() {
      parent::__construct();

      add_action('login_head', array($this, 'enqueuePersona'));
    }

    /**
     * Gets a verified user. Used to process an authentication response.
     */
    protected function getVerifiedUser() {
      // Determine the audience.
      $audience = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') ? 'https://' : 'http://';
      $audience .= $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'];

      $postdata = 'assertion=' . urlencode($_POST['assertion']) . '&audience=' . urlencode($audience);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, "https://verifier.login.persona.org/verify");
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
      $response = curl_exec($ch);
      curl_close($ch);

      $result = json_decode($response);
      if ($result->status === 'okay') {
        return $result->email;
      } else {
        return new WP_Error('persona', __('<strong>ERROR</strong> Unable to verify account with Persona.'));
      }
    }

    /**
     * Enqueues the persona script.
     */
    public function enqueuePersona() {
      wp_enqueue_script('mozilla-persona', 'https://login.persona.org/include.js', array('jquery'));
    }

    /**
     * Gets a name that identifies this plugin. Should be unique from all other
     * authentication plugins. Used in various functions to identify this plugin.
     * It is also used as a query variable and should be so compatible.
     */
    protected function getName() {
      return "PersonaAuth";
    }

    /**
     * Gets the content to be put on the login page.
     */
    protected function getLoginFormContent() {
      ob_start();
      ?>

      <div id="PersonaAuthDiv">
        <a id="PersonaAuth_url" href="#" rel="nofollow">
          <img style="display: block; margin: auto"
               src="<?php echo esc_url(plugins_url('img/PersonaLogin.png', __FILE__)) ?>"/>
        </a>
      </div>
      <form name="PersonaAuthForm" id="PersonaAuthForm" style="display: none;" method="post"
            action="<?php echo esc_url($this->getAuthenticateUrl()); ?>">
        <input type="hidden" name="assertion" id="persona-assertion"/>
      </form>

      <script>
        // Clicking the link verifies our identity and submits the link.
        jQuery('div#PersonaAuthDiv a#PersonaAuth_url').on('click', function () {
          navigator.id.get(function (assertion) {
            if (assertion != null) {
              jQuery('form#PersonaAuthForm input[name="assertion"]').val(assertion);
              jQuery('form#PersonaAuthForm').trigger('submit');
            }
          });
          return false;
        });

        // Grab redirect_to from the main form.
        jQuery(document).ready(function () {
          jQuery('form#loginform input[name="redirect_to"]').clone().appendTo(jQuery('form#PersonaAuthForm'));
        });
      </script>

      <?php
      return ob_get_clean();
    }

    /**
     * Authenticates the user.
     */
    protected function authenticateUser($user, $username = null, $password = null) {
      $pInfo = $this->getVerifiedUser();
      if (is_wp_error($pInfo)) {
        return $pInfo;
      }
      $user = get_user_by('email', $pInfo);
      if (!$user) {
        return new WP_Error('persona',
            __('<strong>ERROR</strong>: No user is registered with the selected Persona email.'));
      }
      return $user;
    }

    /**
     * Gets the content to be put on the registration page.
     */
    protected function getRegisterFormContent() {
      // Otherwise return the login form.
      if (isset($_POST['user_login'])) {
        $user_login = $_POST['user_login'];
      }

      ob_start();
      ?>

      <form name="PersonaAuthForm" id="PersonaAuthForm" action="<?php echo esc_url($this->getRegisterUrl()); ?>"
            method="post">
        <p>
          <label for="user_login"><?php _e('Username') ?><br/></label>
          <input type="text" name="user_login" id="persona_auth_user_login" class="input"
                 value="<?php echo esc_attr(wp_unslash($user_login)); ?>" size="20"/>
        </p>
        <a id="PersonaAuth_url" href="#" rel="nofollow">
          <img style="display: block; margin: auto"
               src="<?php echo esc_url(plugins_url('img/PersonaRegister.png', __FILE__)) ?>"/>
        </a>
        <input type="hidden" name="assertion" id="persona-assertion"/>
      </form>

      <script>
        // Prevent enter from submitting the form.
        jQuery('form#PersonaAuthForm input[name="user_login"]').on('keypress', function (e) {
          if (e.which == 13) {
            console.log('Hello');
            e.preventDefault();
            return false;
          }
        });

        // Clicking the link verifies our identity and submits the link.
        jQuery('form#PersonaAuthForm a#PersonaAuth_url').on('click', function () {
          navigator.id.get(function (assertion) {
            if (assertion != null) {
              jQuery('form#PersonaAuthForm input[name="assertion"]').val(assertion);
              jQuery('form#PersonaAuthForm').trigger('submit');
            }
          });
          return false;
        });

        // Grab redirect_to from the main form.
        jQuery(document).ready(function () {
          jQuery('form#loginform input[name="redirect_to"]').clone().appendTo(jQuery('form#PersonaAuthForm'));
        });
      </script>

      <?php
      return ob_get_clean();
    }

    /**
     * Authenticates the user with this authentication method.
     */
    protected function registerUser($username, $usermail, $errors) {
      // If there is a problem with the username we cannot register it.
      $newErrors = new WP_Error();
      foreach (array('empty_username', 'invalid_username', 'username_exists') as $code) {
        $this->copyErrors($errors, $newErrors, $code);
      }
      if ($newErrors->get_error_code() != '') {
        return $newErrors;
      }

      $pInfo = $this->getVerifiedUser();
      if (is_wp_error($pInfo)) {
        return $pInfo;
      }

      $user = get_user_by('email', $pInfo);
      if ($user) {
        return new WP_Error('persona',
            __('<strong>ERROR</strong>: A user is already associated with the selected Persona email.'));
      }

      $usermail = $pInfo;
      $userpass = wp_generate_password(12, false);
      $userId = wp_create_user($username, $userpass, $usermail);
      if (is_wp_error($userId)) {
        if ($userId->get_error_code() == 'existing_user_email') {
          return new WP_Error('persona', 'The selected Persona email is already in use on this site.');
        } else {
          return $userId;
        }
      }
      if (!$userId) {
        return new WP_Error('registerfail',
            sprintf(__('<strong>ERROR</strong>: Couldn\'t register you&hellip; please contact the ' .
                '<a href="mailto:%s">webmaster</a> !'), get_option('admin_email')));
      }
      return $userId;
    }

    /**
     * Adds a short method describing how to use Mozilla personas.
     */
    protected function getProfileContent($user) {
      ob_start();
      ?>

      <label for="email"><h2 style="color: red;">Persona Authentication</h2></label>
      <table class="form-table">
        <tbody>
        <tr>
          <th scope="row"><label for="email"><?php _e("Persona Account") ?></label></th>
          <td>
            Persona will use your email address to authenticate you. Please ensure that your account's email address
            (set below) matches the email address you wish to use for authentication.
            <br/>
          </td>
        </tr>
        </tbody>
      </table>

      <script>
        jQuery(function () {
          jQuery('label[for="email"]').css('color', 'red').css('font-size', '18px');
        });
      </script>

      <?php
      return ob_get_clean();
    }

    /**
     * Not used in Persona.
     */
    protected function updatePersonalOptions($userId, $errors) { }

  }
}

/**
 * Starts BasicAuth.
 */
function PersonaAuth() {
  return PersonaAuth::getInstance();
}

PersonaAuth();

?>
