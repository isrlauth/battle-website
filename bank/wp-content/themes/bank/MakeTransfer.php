<?php /* Template Name: Make Transfer */ ?>

<?php get_header(); ?>

<?php
// Get the values we will show in the page.

$user = wp_get_current_user();

// Try to transfer the funds as requested.
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $senderId = $user->id;
	$senderType = $_POST['from'];

  $recipient = get_user_by('login', $_POST['toAccount']);
  if ($recipient && !is_wp_error($recipient)) {
    $recipientId = $recipient->id;
    $recipientType = $_POST['toType'];

    $amount = intval($_POST['amount']);
    $memo = $_POST['memo'];

    if ($amount <= 0) {
      $error = "Amount must be greater than 0.";
    } elseif (is_null($recipientId)) {
      $error = "Invalid recipient account number.";
    } else {
      $success = $wpdb->insert("{$wpdb->prefix}transactions",
        array(
          'sender_id' => $senderId,
          'sender_type' => $senderType,
          'recipient_id' => $recipientId,
          'recipient_type' => $recipientType,
          'amount' => $amount,
          'description' => 'The code to finish this task is ' . rand() . '.' //$memo
        ),
        array(
          '%s', 
          '%s',
          '%s',
          '%s',
          '%.2f',
          '%s'
        ));

      if (!$success) {
        if (strpos($wpdb->last_error, 'Cannot insert transaction that will put a balance below 0.') !== false) {
          $error = "You don't have sufficient funds to make this transfer.";
        } elseif (strpos($wpdb->last_error, 'Cannot transfer money to the same account.') !== false) {
          $error = "Can't transfer money from one account to the same account.";
        } elseif (strpos($wpdb->last_error, 'Cannot insert transaction with an amount less than or equal to zero.') !== false) {
          $error = "Amount must be greater than 0.";
        }
      }
    }
  } else {
    $error = "Invalid recipient account.";
  }
}

?>

<form id="transferForm" method="post" action="">
<div style="width:600px">
	<style>
		.transferRow {height: 50px; line-height: 50px;}
		.transferLeft {width: 150px; float: left; text-align:left; }
		.transferRight {width: 450px; float: right; text-align:left; }
	</style>

	<h1>Transfer Funds</h1>

	<div class="transferRow">
		<label class="transferLeft" for="from">From:</label>
		<div class="transferRight">
			<select name="from" style="width: 125px;">
				<option value="savings" <?php if ($_POST['from'] !== 'Checking') { echo "selected='selected'"; } ?>>Savings</option>
				<option value="checking"<?php if ($_POST['from'] === 'Checking') { echo "selected='selected'"; } ?>>Checking</option>
			</select>
		</div>
	</div>

	<div class="transferRow">
		<label class="transferLeft" for="toAccount">To:</label>
		<div class="transferRight">
			<input style="width: 125px;" id="toAccount" name="toAccount" type="text" placeholder="Account Number"
				value="<?php echo $_POST['toAccount']; ?>" />
			<select name="toType" style="width: 125px; margin-left: 15px;">
				<option value="savings" <?php if ($_POST['toType'] !== 'Checking') { echo "selected='selected'"; } ?>>Savings</option>
				<option value="checking"<?php if ($_POST['toType'] === 'Checking') { echo "selected='selected'"; } ?>>Checking</option>
			</select>
		</div>
	</div>

	<div class="transferRow">
		<label class="transferLeft" for="amount">Amount:</label>
		<div class="transferRight"><input style="width: 125px;" id="amount" name="amount" type="text" placeholder="$0.00"
			value="<?php echo $_POST['amount']; ?>" /></div>
	</div>


	<div class="transferRow">
		<label class="transferLeft" for="memo">Memo:</label>
		<div class="transferRight"><input name="memo" type="text" placeholder="Description of transfer"i
			value="<?php echo $_POST['memo']; ?>" /></div>
	</div>

	<br clear="all" />

	<?php if ($success) : ?>
		<h2>The transfer was successful.</h2>
		<script>
			jQuery("#transferForm :input").prop("disabled", true);	
		</script>
	<?php else : ?>	
		<div>
			<div class="transferLeft"><input type="submit" /></div>
			<div class="transferRight">
				<?php if ($error) : ?>
					<label class="error" style="color: red; width: 450px;"><?php echo $error; ?></label>
				<?php endif; ?>
				<label for="toAccount" class="error" generated="true" style="color: red; width: 450px;"></label></br />
				<label for="amount" class="error" generated="true" style="color: red; width: 450px;"></label>
			</div>
		</div>
	<?php endif; ?>

</div>
</form>

<br /><br />

<script>
	(function($) {
		$(document).ready(function() {
			$('#transferForm').validate();
			
			$('#toAccount').rules('add', {
				required: true,
				//number: true,
				//rangelength: [6, 6],
				messages: {
					required: "Please enter a destination account number."//,
					//number: "Please enter a valid six digit destination account number.",
					//rangelength: "Please enter a valid six digit destination account number."
				}
			});

			$('#amount').rules('add', {
				required: true,
				number: true,
				range: [0.01, 999999.99],
				messages: {
					required: "Please enter an amount to transfer.",
					number: "Please enter a valid amount between $0.01 and $999,999.99.",
					range: "Please enter a valid amount between $0.01 and $999,999.99."
				}
			});


		});
	})(jQuery);
</script>


<?php get_footer(); ?>

