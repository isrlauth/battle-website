<?php /* Template Name: Transaction History */ ?>

<?php get_header(); ?>

<?php
// Get the values we will show in the page.
$user = wp_get_current_user();

$account_type_num = get_query_var('account_type');
$account_type = $account_type_num === '1' ? 'Checking' : 'Savings';

$transactions = $wpdb->get_results(
  "SELECT t.transaction_timestamp AS timestamp,
          IF(t.sender_id = {$user->id} AND (t.sender_id <> t.recipient_id OR t.sender_type = '$account_type'), 'Debit', 'Credit') AS type,
          IF(t.sender_id = {$user->id} AND (t.sender_id <> t.recipient_id OR t.sender_type = '$account_type'), r.user_login, s.user_login) AS other_account,
          IF(t.sender_id = {$user->id} AND (t.sender_id <> t.recipient_id OR t.sender_type = '$account_type'), t.recipient_type, t.sender_type) AS other_type,
          IF(t.sender_id = {$user->id} AND (t.sender_id <> t.recipient_id OR t.sender_type = '$account_type'), t.sender_balance, t.recipient_balance) AS balance,
          t.description AS memo, t.amount AS amount, t.sender_id = t.recipient_id as is_self_transfer
	 FROM {$wpdb->prefix}transactions t
	 JOIN $wpdb->users s ON (t.sender_id = s.ID)
	 JOIN $wpdb->users r ON (t.recipient_id = r.id)
	 WHERE (t.sender_id = {$user->id} AND t.sender_type = '$account_type')
		  OR (t.recipient_id = {$user->id} AND t.recipient_type = '$account_type')
	 ORDER BY `timestamp` DESC;");

	#echo "<pre>";
	#print_r("");
  #echo "</pre>";
?>

<div style="width:100%; margin-left: auto; margin-right: auto;">
	<style>
		tr:nth-child(odd) { background-color:#fff; }
		.moneyCell { width: 80px; text-align: right; }
	</style>

	<h1><?php echo $account_type; ?></h1>

	<table id="transactions" style="width: 100%;">
		<thead>
			<tr>
				<th style="width: 150px;">Date</th>
				<th style="width: 260px;">Description</th>
				<th>Memo</th>
				<th class="moneyCell">Debit</th>
				<th class="moneyCell">Credit</th>
				<th class="moneyCell">Balance</th>
			</tr>
		</thead>

		<tbody>
		<?php foreach($transactions as $transaction): ?>
			<tr>
				<td><?php echo date('m/d/Y h:i A', strtotime($transaction->timestamp)); ?></td>
				<td>
        <?php
          
          if ($transaction->type === 'Debit') {
            if ($transaction->is_self_transfer) {
              echo 'Transferred funds to ' . strtolower($transaction->other_type);
            } else {
              printf('Sent funds to account #%d', $transaction->other_account);
            }
          } else {
            if ($transaction->is_self_transfer) {
              echo 'Transferred funds from ' . strtolower($transaction->other_type);
            } else {
              printf('Received funds from account #%d', $transaction->other_account);
            }
					}
				?>
				<td><?php echo $transaction->memo; ?></td>
				<td class="moneyCell"><?php if ($transaction->type === 'Debit') { printf('$%.2f', $transaction->amount); } ?></td>
				<td class="moneyCell"><?php if ($transaction->type === 'Credit') { printf('$%.2f', $transaction->amount); } ?></td>
				<td class="moneyCell"><?php printf('$%.2f', $transaction->balance); ?></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>

<?php get_footer(); ?>

