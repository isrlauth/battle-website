<?php /* Template Name: Account Details */ ?>

<?php get_header(); ?>

<?php
// Get the values we will show in the page.

$user = wp_get_current_user();

$transactionUrl = get_page_link(get_page_by_title('transactions')->ID) . '?account_type=%d';

?>

<div style="width:100%; margin-left: auto; margin-right: auto;">
	<h1> Account #<?php echo $user->user_login; ?></h1>

	<style>
		.rightColumn { text-align: right; width: 125px; margin-left: 10px; }
	</style>

	<table style="width: 100%;">
		<thead>
			<tr>
				<th>Account</th>
				<th class="rightColumn">Funds</th>
			</tr>
		</thead>

		<tbody>
			<tr>
				<td><a href="<?php printf($transactionUrl, 0); ?>">Savings</a></td>
				<td class="rightColumn">$<?php echo number_format($user->savings_balance, 2); ?></td>
			</tr>
			<tr>
				<td><a href="<?php printf($transactionUrl, 1); ?>">Checking</a></td>
				<td class="rightColumn">$<?php echo number_format($user->checking_balance, 2); ?></td>
			</tr>
		</tbody>
	</table>
</div>

<?php get_footer(); ?>

