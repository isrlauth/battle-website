<?php
/*
	Template Name: Front Page
	D5 Business Line Theme's Front Page to Display the Home Page if Selected
	Copyright: 2012, D5 Creation, www.d5creation.com
	Based on the Simplest D5 Framework for WordPress
	Since D5 Business Line 1.0
*/
?>

<?php get_header(); ?>

<h1> Welcome to the Bank of the Test!</h1>

<br />
<?php if (is_user_logged_in()):
  get_currentuserinfo();
?>
  Welcome <?php echo $current_user->user_firstname . " " . $current_user->user_lastname; ?>.
  Please use the links in the upper right corner of the page to manage your account. 
<?php else: ?>
  Please <a href="<?php echo wp_login_url('/'); ?>">login</a> to access your account.
<?php endif; ?>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
