<?php

  /**
   * Plugin Name: Bank Tester
   * Plugin URI: http://isrlresearch.byu.edu/
   * Description: A plugin that helps us test authentication on our forums. This plugin requires
   * that there is an administrative user named admin.
   * Author: Scott Ruoti
   * Author URI: http://isrl.byu.edu.com/
   * License: None
   * Network: true
   * Text Domain: formus-tester
   * Domain Path: /lang
   */

if (!class_exists('BankTester')) {
class BankTester
 {
  
  /**
   * Singleton code.
   */
  private static $instance = null;
  public static function get_instance() {
    if (null == self::$instance) {
      self::$instance = new self;
    }
    return self::$instance;
  }

  /**
   * Constructer.
   */
  function __construct() {
    add_action('parse_request', array($this, 'logoutUser'));
    add_action('parse_request', array($this, 'deleteUser'));
    add_action('parse_request', array($this, 'createUser'));
    add_action('parse_request', array($this, 'lookupUser'));
    add_action('parse_request', array($this, 'transferFunds'));

    add_action('wp_login', array($this, 'userLoggedIn'),10, 2);

    add_filter('login_redirect', array($this, 'loginRedirect'), 10, 3);

    add_action('profile_personal_options', array($this, 'checkUserProfileSetup'), 1);
    add_filter('user_profile_update_errors', array($this, 'personalOptionsErrors'), 10, 3);
  }

  /**
   * Sends a response that can be done cross domain.
   */
  private function sendCorsResponse($content = null) {
    if (isset($_SERVER['HTTP_ORIGIN'])) {
      header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
      header("Access-Control-Expose-Headers: x-json");
      header('Access-Control-Allow-Credentials: true');
    } else
      header('Access-Control-Allow-Origin: *');

    if ($content) {
      header('Content-Type: application/json');
      echo $content;
    }
      
    exit;
  }


  /**
   * Log the user out.
   */
  public function logoutUser() {
    if(isset($_GET['logoutUser'])) {
      wp_logout();
      $this->sendCorsResponse();
    }
  }

  public function deleteUser() {
    if(isset($_GET['deleteUser'])) {
      $wpSession = WP_Session::get_instance();
      if(isset($wpSession['BankTester_userId']))
        $user = get_userdata($wpSession['BankTester_userId']);
      if(!($user && !is_wp_error($user)))
        $user = wp_get_current_user();

      // If we have a valid user back them up.
      if($user && !is_wp_error($user) && $user->user_login != 'admin') {
        require_once(ABSPATH . 'wp-admin/includes/user.php');
        wp_delete_user($user->id);
        $this->sendCorsResponse();
      }
    } 
  }

  /**
   * Log the user out.
   */
  public function createUser() {
    if(isset($_GET['createUser'])) {
      $bankerNumber = intval(get_option('BankTester_accountNumber', '100000'));
      $bankerNumber += 1;
      update_option('BankTester_accountNumber', $bankerNumber);

      $userpass = wp_generate_password(12, false);
      $userId = wp_create_user("{$bankerNumber}", $userpass);
      add_user_meta($userId, 'user_password', $userpass, true);

      $wpSession = WP_Session::get_instance();
      $wpSession['BankTester_userId'] = $userId;

      $this->sendCorsResponse("{\"AccountNumber\": \"{$bankerNumber}\", \"Password\": \"{$userpass}\"}");
    }
  }

  /**
   * Log the user out.
   */
  public function lookupUser() {
    if(isset($_GET['lookupUser'])) {
      if(isset($wpSession['BankTester_userId']))
        $user = get_userdata($wpSession['BankTester_userId']);
      if(!($user && !is_wp_error($user)))
        $user = wp_get_current_user();

      error_log(!!$user ? 'True' : 'False');

      // Transfer money to the admin.
      if($user && !is_wp_error($user) && $user->user_login != 'admin') {
        $userpass = get_user_meta($user->ID, 'user_password', true);
        $this->sendCorsResponse("{\"AccountNumber\": \"{$user->nickname}\", \"Password\": \"{$userpass}\"}");
      } else {
        $this->sendCorsResponse("");
      }
    }
  }

  /**
   * Transfer funds to the user saved in the session.
   */
  public function transferFunds() {
    if(isset($_GET['transferFunds'])) {
      $wpSession = WP_Session::get_instance();
      if(isset($wpSession['BankTester_userId']))
        $user = get_userdata($wpSession['BankTester_userId']);
      if(!($user && !is_wp_error($user)))
        $user = wp_get_current_user();

      // Transfer money to the admin.
      if($user && !is_wp_error($user) && $user->user_login != 'admin') {
        $admin = get_user_by('login', 'admin');

        // Transfer money from admin to the user.
        global $wpdb;
        $success = $wpdb->insert("{$wpdb->prefix}transactions",
          array(
            'sender_id' => $admin->id,
            'sender_type' => 'Checking',
            'recipient_id' => $user->id,
            'recipient_type' => 'Checking',
            'amount' => rand(1000, 9999),
            'description' => "User study transfer."
          ),
          array(
            '%d', 
            '%s',
            '%d',
            '%s',
            '%.2f',
            '%s'
          ));

        // Restore the admin's money.
        $wpdb->update($wpdb->users, 
          array(
            'savings_balance' => 1000000000,
            'checking_balance' => 1000000000
          ),
          array('ID' => $admin->id));
        $this->sendCorsResponse();
      }
    }
  }

  /**
   * Send users an email with the code they need to complete the first forum task.
   */
  public function userLoggedIn($userLogin, $user) {
    $wpSession = WP_Session::get_instance();
    $wpSession['BankTester_userId'] = $user->id;
  }

  /**
   * Redirect the user to the main page.
   */
  public function loginRedirect($redirectTo, $request, $user) {
    return home_url();
  }

  /**
   *
   */
  public function checkUserProfileSetup($user) {
    $wpSession = WP_Session::get_instance();
    $setupComplete = False;

    // Hard code our various states. There might be better ways, but I couldn't think of any without
    // trade offs.
    if(isset($wpSession["FacebookAuth_enabled"]) && intval(get_user_meta($user->id, "FacebookAuth_id", True)))
      $setupComplete = True;
    else if(isset($wpSession["AugmentedGoogleAuth_enabled"]) && intval(get_user_meta($user->id, "AugmentedGoogleAuth_id", True)))
      $setupComplete = True; 
    else if(isset($wpSession["GoogleAuth_enabled"]) && intval(get_user_meta($user->id, "GoogleAuth_id", True)))
      $setupComplete = True; 
    else if(isset($wpSession["PersonaAuth_enabled"]) && is_email($user->user_email))
      $setupComplete = True;
    else if(isset($wpSession["SawAuth_enabled"]) && is_email($user->user_email))
      $setupComplete = True;
    else if(isset($wpSession["HatchetAuth_enabled"]) && is_email($user->user_email))
      $setupComplete = True;
    else if (isset($wpSession["PasswordAuth_enabled"]))
      $setupComplete = True;

    $code = rand();
    while ($code < 1000000000)
      $code = rand();

    if ($setupComplete) {
      ?>
      <div id="message" class="updated">
        <p><strong>
          <?php echo __('Account setup! The code to finish the task is ') . $code ; ?>
        </strong></p>
      </div>
      <?php
    }
  }

  /**
   * Ignore the email error since I have created accounts with blank emails.
   */
  public function personalOptionsErrors($errors, $update, $user) {
    $newErrors = new WP_Error();  
    foreach($errors->get_error_codes() as $code) {
      foreach($errors->get_error_messages($code) as $item) {
        if ($code != 'empty_email')
          $newErrors->add($code, $item, $errors->get_error_data($code));
      }
    }

    $errors = $newErrors;
  }

}
}

/**
 * Starts BankTester.
 */
function BankTester() {
  return BankTester::get_instance();
}
BankTester();

/*
Plugin Name: Disable Lost Password Feature
*/
function disable_password_reset() { return false; }
add_filter ( 'allow_password_reset', 'disable_password_reset' );
add_filter ( 'show_password_fields', 'disable_password_reset' );

?>
